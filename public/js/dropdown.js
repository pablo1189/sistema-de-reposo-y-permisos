/*
** Actualiza los departamentos
*/

$(document).ready(function() {
$("#division").change(function(event){
  $.get("consulta/departamentos/"+event.target.value+"", function(response, division){
  console.log(response);
    $("#departamento").empty();
    $("#departamento").append("<option value=''> --Seleccione --</option>");
  for (var i = 0; i < response.length; i++) {
      $("#departamento").append("<option value='"+response[i].id+"'> "+response[i].nombre+"</option>");
    }
  });
});
});


/*
** Actualiza los funcionarios
*/

$(document).ready(function() {
$("#division").change(function(event){
  $.get("consulta/funcionarios/"+event.target.value+"", function(response, division){
  console.log(response);
    $("#funcionario").empty();
    $("#funcionario").append("<option value=''> --Seleccione --</option>");
  for (var i = 0; i < response.length; i++) {
      $("#funcionario").append("<option value='"+response[i].id+"'> "+response[i].nombre_completo+"</option>");
    }
  });
});
});


