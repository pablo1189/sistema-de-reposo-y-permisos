<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Permiso;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        view()->share('permiso', Permiso::where('status', 1)->count());

        view()->share('evaluacion', Permiso::where('status', 0)->count());

        view()->share('activo', Permiso::where('status', 1)->count());
     
        
     }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
