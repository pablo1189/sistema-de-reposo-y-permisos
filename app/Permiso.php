<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permiso extends Model
{
    protected $table ='permisos';

    protected $fillable = ['funcionario_id', 'tipo_id','fecha_inicio','fecha_final','cantidad_hora','hora_inicio','hora_final','observacion','status','img_scanner'];

    public function funcionario () {
    	return $this->belongsTo('App\Funcionario');
    }

    public function tipo () {
    	return $this->belongsTo('App\Tipo');
    }

    /*
    Escopes de Permisos
    */


    public function scopeDivisiones($query, $division_id){

        if (!empty($division_id)) { 
            /*return $query->join('divisions', function($join) use ($division_id)
            {
                $join->on('divisions.id', '=', 'departamentos.division_id')
                ->on('departamentos.id', '=', 'funcionarios.departamento_id')
                 ->on('funcionarios.id', '=', 'permisos.funcionario_id')
                ->where('division_id', $division_id);*/


            return $query->join('funcionarios', function($join) use ($division_id)
            {
                $join->on('funcionarios.id', '=', 'permisos.funcionario_id')
                ->where('division_id', $division_id);
            });
        }
    }

public function scopeDepartamentos($query, $departamento){

        if (!empty($departamento)) {

            return $query->join('departamentos', function($join) use ($departamento)
            {
                $join->on('departamentos.id', '=', 'funcionarios.departamento_id')
                 ->on('funcionarios.id', '=', 'permisos.funcionario_id')
                ->where('departamento_id', $departamento);
            });
        }
    }
    

/*
   	public function scopeDepartamentos($query, $departamento_id){

        if (!empty($departamento_id)) {
            return $query->join('funcionarios', function($join) use ($departamento_id)
            {
                $join->on('funcionarios.id', '=', 'permisos.funcionario_id')
               ->where('departamento_id',$departamento_id);
            });
        }
    }
    */

    public function scopeFuncionarios($query, $funcionario) {
        if (!empty($funcionario)) {
            $query->where('funcionario_id', '=', $funcionario);
        }
    }


    public function scopeYears($query, $year) {
        if (!empty($year)) {
            $query->whereYear('fecha_final', '=', $year)->orderBy('fecha_final', 'ASC');
            
        }
    }


 public function scopeFechas($query, $fecha) {
        if (!empty($fecha)) {
            $query->whereDate('fecha_inicio', '=', $fecha)->orderBy('fecha_inicio', 'ASC');
            
        }
    }

 public function scopeInactivo ($query)
 {
    return $query->where('status', 0);
 }    

}
