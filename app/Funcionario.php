<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Funcionario extends Model
{
    protected $table ='funcionarios';

    protected $fillable = ['cedula','nombre','apellido', 'nombre_completo','cargo','division_id','departamento_id','telefono', 'foto', 'user_id'];

    public function division () {
    	return $this->belongsTo('App\Division');
    }

    public function departamento () {
    	return $this->belongsTo('App\Departamento');
    }

     public function permiso () {
    	return $this->hasMany('App\Permiso');
    }

    public function user () {
        return $this->hasOne('App\User');
    }

     public static function funcionarios($id)
    {
        return Funcionario::where('division_id', '=', $id)->get();
    }
}
