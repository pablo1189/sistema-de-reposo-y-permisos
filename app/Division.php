<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Division extends Model
{
    protected $table ='divisions';

    protected $fillable = ['nombre', 'descripcion'];

       public function funcionario () {
    	return $this->hasMany('App\Funcionario');
    }

  
       public function departamento () {
    	return $this->hasMany('App\Departamento');
    }

}
