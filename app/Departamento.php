<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    protected $table ='departamentos';

    protected $fillable = ['division_id','nombre', 'descripcion'];

       public function division () {
    	return $this->belongsTo('App\Division');
    }

    
     public function funcionario () {
    	return $this->hasMany('App\Funcionario');
    }

    public static function departamentos($id)
    {
        return Departamento::where('division_id', '=', $id)->get();
    }

}
