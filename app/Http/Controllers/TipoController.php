<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Tipo;

class TipoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipos= Tipo::all();
        return view('tipos.index', compact('tipos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tipos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $data =
        [
            'nombre'=>$request->get('nombre'),
            'descripcion'=>$request->get('descripcion')
        ];

        $data_save=Tipo::create($data);

        $message= $data_save ? 'Registro Agregado Exitosamente' : 'Error Intente Nuevamente..';

        return redirect()->route('tipos.index')->with('message',$message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       #return view('tipos.show', ['nombre'=>Tipo::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipo = Tipo::where('id', $id)->first();
       return view('tipos.edit', compact('tipo'));
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            $tipo = Tipo::findOrFail($id);
            
            $tipo->nombre = $request->get('nombre');
            $tipo->descripcion = $request->get('descripcion');
            
            $updated=$tipo->update();

            $message=$updated ? 'Tipo de Permiso actualizado' : 'El tipo de Permiso NO pudo ser actualizado';
            return Redirect::to('tipos')->with('message', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tipo = Tipo::where('id', $id)->first();
        $deleted = $tipo->delete();
        $message = $deleted?'Registro eliminado correctamente' : 'Error al Eliminar';
        return redirect()->route('tipos.index')->with('message', $message);
    }
}
