<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Departamento;
use App\Division;


class DepartamentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departamentos= Departamento::all();
        
        return view('departamentos.index', compact('departamentos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $divisiones= Division::orderBy('nombre','ASC')->pluck('nombre','id');
        return view('departamentos.create',compact('divisiones'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data =
        [
            'division_id'=>$request->get('division_id'),
            'nombre'=>$request->get('nombre'),
            'descripcion'=>$request->get('descripcion')

        ];

        $data_save=Departamento::create($data);

        $message= $data_save ? 'Registro Agregado Exitosamente' : 'Error Intente Nuevamente..';


        return redirect()->route('departamentos.index')->with('message',$message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $departamento= Departamento::where('id',$id)->first();
        
        return view('departamentos.show',compact('departamento')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $departamento = Departamento::where('id', $id)->first();
        $division = Division::orderBy('nombre', 'ASC')->pluck('nombre','id');
        return view('departamentos.edit', compact('departamento', 'division'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $departamento = Departamento::where('id', $id)->first();
      $departamento->fill($request->all());
      $updated = $departamento->save();
      $message = $updated?'Registro actualizado correctamente.':'Error, intente nuevamente';
      return redirect()->route('departamentos.index')->with('message', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $departamento = Departamento::where('id', $id)->first();
        $deleted = $departamento->delete();
        $message = $deleted?'Registro eliminado correctamente' : 'Error al Eliminar';
        return redirect()->route('departamentos.index')->with('message', $message);
    }
}
