<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use File;
use App\Permiso;
use App\Funcionario;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        

        $evaluacion = Permiso::where('status', 0)->count();

        $activo = Permiso::where('status', 1)->count();

        return view('home',compact('evaluacion', 'activo'));
    }
}
