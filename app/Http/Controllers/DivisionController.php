<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Division;


class DivisionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $divisions= Division::all();
        return view('divisions.index', compact('divisions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('divisions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $data =
        [
            'nombre'=>$request->get('nombre'),
            'descripcion'=>$request->get('descripcion')
        ];

        $data_save=Division::create($data);

        $message= $data_save ? 'Registro Agregado Exitosamente' : 'Error Intente Nuevamente..';

        return redirect()->route('divisions.index')->with('message',$message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $division = Division::where('id', $id)->first();
       return view('divisions.edit', compact('division'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $division=Division::findOrFail($id);
            $division->nombre=$request->get('nombre');
            $division->descripcion=$request->get('descripcion');
            $updated=$division->update();

            $message=$updated ? 'Division actualizada' : 'Division NO pudo ser actualizada';
            return Redirect::to('divisions')->with('message', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $division = Division::where('id', $id)->first();
        $deleted = $division->delete();
        $message = $deleted?'Registro eliminado correctamente' : 'Error al Eliminar';
        return Redirect::to('divisions')->with('message', $message);
    }
}
