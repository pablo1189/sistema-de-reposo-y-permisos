<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use File;
use PDF;
use Auth;
use App\Division;
use App\Permiso;
use App\Funcionario;
use App\Tipo;
use App\Departamento;



class PermisoController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        #$permisos= Permiso::all();
        #   
        $date = Carbon::now();
 
        $date = $date->format('h:i:s A');

        $fecha_actual = Carbon::today();

        

        $permisos = Permiso::orderBy('id','Desc')->get();
        
        foreach ($permisos as $key => $value) {

            $fecha_final = $value->fecha_final;

            if ( $fecha_final <= $fecha_actual ) {
                
                $value->status = 0;
                $value->save();
            }
        }

        return view('permisos.index', compact('permisos', 'fecha_actual', 'estatus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::User()->rol == 1) {
        
        $funcionario = Funcionario::orderBy('nombre','ASC')->pluck('nombre_completo','id');

        $tipo = Tipo::orderBy('nombre','ASC')->pluck('nombre','id');

        $divisiones= Division::orderBy('nombre','ASC')->pluck('nombre','id');

        $departamentos= Departamento::orderBy('nombre','ASC')->pluck('nombre','id');

        
        return view('permisos.create',compact('funcionario', 'tipo', 'divisiones', 'departamentos'));

        }else{

        $user_id = Auth::id();

        $funcionario = Funcionario::where('user_id', $user_id)->orderBy('nombre_completo', 'ASC')->pluck('nombre_completo','id');

        
        $tipo = Tipo::orderBy('nombre','ASC')->pluck('nombre','id');

        $divisiones= Division::orderBy('nombre','ASC')->pluck('nombre','id');

        $departamentos= Departamento::orderBy('nombre','ASC')->pluck('nombre','id');

        return view('permisos.create',compact('funcionario', 'tipo', 'divisiones', 'departamentos'));
      }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $this->validate($request, [
            'imagen'=>'max:2000|mimes:jpeg,png']);
            $destination='uploads/permisos';
            $imagen=$request->file('img_scanner');
            $random= str_random(6);


        if (!empty($imagen)) {
            $filename    = $random . $imagen->getClientOriginalName(); // get the filename
            $imagen->move($destination, $filename); // move file to destination
        }else{
            $filename = '';
        }
         $data =
        [
            'funcionario_id'=>$request->get('funcionario_id'),
            'tipo_id'=>$request->get('tipo_id'),
            'fecha_inicio'=>$request->get('fecha_inicio'),
            'fecha_final'=>$request->get('fecha_final'),
            'cantidad_hora'=>$request->get('cantidad_hora'),
            'hora_inicio'=>$request->get('hora_inicio'),
            'hora_final'=>$request->get('hora_final'),
            'observacion'=>$request->get('observacion'),
            'status'=>$request->get('status'),
            'img_scanner'=>$filename,
        ];

        $data_save=Permiso::create($data);

        $message= $data_save ? 'Registro Agregado Exitosamente' : 'Error Intente Nuevamente..';

        return redirect()->route('resultado')->with('message',$message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $permiso = Permiso::where('id',$id)->first();
        $fecha_actual = Carbon::today();
        return view('permisos.show',compact('permiso', 'fecha_actual')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    public function edit($id)
    {
        $permiso= Permiso::where('id', $id)->first();
        $funcionario = Funcionario::orderBy('nombre','ASC')->pluck('nombre_completo','id');
        $tipo = Tipo::orderBy('nombre','ASC')->pluck('nombre','id');
        return view('permisos.edit',compact('permiso','funcionario', 'tipo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $permiso = Permiso::where('id', $id)->first();
        $old_image = $permiso->img_scanner;

        $destination = 'uploads/permisos';
        
        $image = $request->file('img_scanner');
        
        $random = str_random(6);

        if (!empty($image)) {
            $filename    = $random . $image->getClientOriginalName(); // get the filename
            $image->move($destination, $filename); // move file to destination
            File::delete($destination . $old_image);
        }else{
            $filename = $old_image;
        }

        $data =
        [
            'funcionario_id'=>$request->get('funcionario_id'),
            'tipo_id'=>$request->get('tipo_id'),
            'fecha_inicio'=>$request->get('fecha_inicio'),
            'fecha_final'=>$request->get('fecha_final'),
            'cantidad_hora'=>$request->get('cantidad_hora'),
            'hora_inicio'=>$request->get('hora_inicio'),
            'hora_final'=>$request->get('hora_final'),
            'observacion'=>$request->get('observacion'),
            'status'=>$request->get('status'),
            'img_scanner'=>$filename,
        ];


      $permiso->fill($data);
      $updated = $permiso->save();
      $message = $updated?'Registro actualizado correctamente.':'Error, intente nuevamente';
      return redirect()->route('permisos.index')->with('message', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $permiso = Permiso::where('id', $id)->first();
        $deleted = $permiso->delete();
        $message = $deleted?'Registro eliminado correctamente' : 'Error al Eliminar';
        return redirect()->route('permisos.index')->with('message', $message);
    }

    /*public function buscar ()
    {
        return view('permisos.buscar');
        #$funcionarios = Funcionario::orderBy('nombre_completo')->pluck('nombre_completo','id');   
        #return view('permisos.buscar', compact('funcionarios'));
    }*/

    public function resultado (Request $request)
    { 

        if(Auth::User()->rol == 1) {
        
        #$funcionarios = User::where('id', Auth::id())->orderBy('nombre_completo', 'ASC')->pluck('nombre_completo','id');
        
        $funcionarios = Funcionario::orderBy('nombre_completo', 'ASC')->pluck('nombre_completo','id');

        $funcionario_id = $request->funcionario_id;


          if (isset($funcionario_id)) {
            
             $funcionario = Funcionario::where('id', $funcionario_id)->first();
            
            $permisos =  Permiso::where('funcionario_id', $funcionario_id)->get();


          }else{

            $permisos = Permiso::orderBy('id','Desc')->get();
          }

        
        
        }else{

          #$funcionarios = Funcionario::orderBy('nombre_completo', 'ASC')->pluck('nombre_completo','id');

          $user_id = Auth::id();

          $funcionarios = Funcionario::where('user_id', $user_id)->orderBy('nombre_completo', 'ASC')->pluck('nombre_completo','id');

          $funcionario = Funcionario::where('user_id', $user_id)->first();

          $permisos = Permiso::where('funcionario_id', $funcionario->id)->get();
          $fecha_actual = Carbon::today();

        }
        
        return view('permisos.resultado', compact('funcionarios', 'permisos', 'funcionario','fecha_actual'));
    }


    public function historial (Request $request)
    { 

        if(Auth::User()->rol == 1) {
        
        #$funcionarios = User::where('id', Auth::id())->orderBy('nombre_completo', 'ASC')->pluck('nombre_completo','id');
        
        $funcionarios = Funcionario::orderBy('nombre_completo', 'ASC')->pluck('nombre_completo','id');

        $funcionario_id = $request->funcionario_id;


          if (isset($funcionario_id)) {
            
             $funcionario = Funcionario::where('id', $funcionario_id)->first();
            
            $permisos =  Permiso::where('funcionario_id', $funcionario_id)->get();


          }else{

            $permisos = Permiso::orderBy('id','Desc')->get();
          }

        
        
        }else{

          #$funcionarios = Funcionario::orderBy('nombre_completo', 'ASC')->pluck('nombre_completo','id');

          $user_id = Auth::id();

          $funcionarios = Funcionario::where('user_id', $user_id)->orderBy('nombre_completo', 'ASC')->pluck('nombre_completo','id');

          $funcionario = Funcionario::where('user_id', $user_id)->first();

          $permisos = Permiso::where('funcionario_id', $funcionario->id)->get();
          $fecha_actual = Carbon::today();

        }
        
        return view('permisos.historial', compact('funcionarios', 'permisos', 'funcionario','fecha_actual'));
    }

    public function consulta (Request $request)
    {     
      
        $funcionario_id = \Request::get('funcionario_id');
        $departamento = \Request::get('departamento_id');
        $division_id = \Request::get('division_id');
        $anio = \Request::get('anio');
        $fecha = \Request::get('fecha');



        $permisos = Permiso::divisiones($division_id)->departamentos($departamento)->years($anio)->fechas($fecha)->get();



    


        /*if (!empty($departamento_id)) {
            
            $departamento = Departamento::where('id', $departamento_id)->first();
            $funcionarios = Funcionario::where('departamento_id', $departamento_id)->get();

              foreach ($funcionarios as $key => $funcionario) {
                $funcionario_id[$key] = $funcionario->id;
              }

              $permisos = Permiso::whereIn('funcionario_id', $funcionario_id)->get();
        }
        
        elseif (!empty($division_id)) {
            $division = Division::where('id', $division_id)->first();
            $funcionarios = Funcionario::where('division_id', $division_id)->get();

            foreach ($funcionarios as $key => $funcionario) {
              $funcionario_id[$key] = $funcionario->id;
            }

            $permisos = Permiso::whereIn('funcionario_id', $funcionario_id)->get();

          }

        elseif (!empty($funcionario_id)){
         
          $funcionarios = Funcionario::where('id', $funcionario_id)->first();
          $permisos =  Permiso::where('funcionario_id', $funcionario_id)->get();
           
        }

        elseif (!empty($anio)) {
            $permisos = Permiso::whereYear('fecha_final', $anio)->orderBy('fecha_final', 'ASC')->get();
        }

        elseif (!empty($fecha)) {
            $permisos = Permiso::whereDate('fecha_inicio',' =', $fecha)->get();
        }

        else{
            $permisos = Permiso::all();
        } */
        
        $departamentos = Departamento::orderBy('nombre', 'ASC')->pluck('nombre','id');
        $divisiones = Division::orderBy('nombre', 'ASC')->pluck('nombre','id');
        $funcionarios = Funcionario::orderBy('nombre', 'ASC')->pluck('nombre_completo','id');
        $date = Carbon::now();
 
        

        return view('permisos.consulta', compact('permisos','divisiones','departamentos','funcionarios'));
    }


    /**
     * Actualiza el select dependiente de subcategorias.
     */
    public function getDepartamentos(Request $request, $id)
    {
        if ($request->ajax()) {
            $departamentos = Departamento::departamentos($id);
            return response()->json($departamentos);
        }
    }

    public function getFuncionarios(Request $request, $id)
    {
        if ($request->ajax()) {
            $funcionarios = Funcionario::funcionarios($id);
            return response()->json($funcionarios);
        }
    }

 
   public function pdf ($id)
    {
        
        $permiso = Permiso::where('id', $id)->first();
        $fecha_actual = Carbon::today();
        $pdf = PDF::loadView('permisos.pdf', ['permiso' => $permiso, 'fecha_actual' => $fecha_actual]);
        return $pdf->download('Permiso_de:'.$permiso->funcionario->nombre_completo.'_'.date('d_m_Y').'.pdf');
    }

    public function pdf2 ($funcionario_id)
    {
         ini_set('max_execution_time',300);
        $funcionario = Funcionario::where('id', $funcionario_id)->first();
        $permiso = Permiso::where('funcionario_id', $funcionario_id)->get();
        $pdf = PDF::loadView('permisos.pdf2', ['permiso' => $permiso, 'funcionario' => $funcionario]);
        return $pdf->download('Permisos_de:'.$funcionario->nombre_completo.'.pdf');
    }

    public function pdf3 ()
    {
         ini_set('max_execution_time',300);
        $permiso = Permiso::all();
        $pdf = PDF::loadView('permisos.pdf3', ['permiso' => $permiso]);
        return $pdf->download('Permisos_DGCSGG.pdf');
    }

    public function pdf4 ($id)
    {
        $division = Division::where('id', $id)->first();
        $departamento = Departamento::where('division_id', $id)->orderBy('departamento_id', 'ASC')->get();
       

        $funcionarios = Funcionario::where('departamento_id', $departamento_id)->get();
        
        
        $permiso = Permiso::divisiones($division)->departamentos($departamento)->get();
        $date = Carbon::now();
        $pdf = PDF::loadView('permisos.pdf4', ['permiso' => $permiso, 'departamentos'=>$departamento,'divisiones'=>$division]);
        return $pdf->download('PERMISOS_DIVISION_DEPARTAMENTO.pdf');
    }
}