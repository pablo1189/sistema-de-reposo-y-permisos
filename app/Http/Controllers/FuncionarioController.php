<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use File;
use App\Funcionario;
use App\Departamento;
use App\Division;
use App\User;


class FuncionarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $funcionarios= Funcionario::all();
        return view('funcionarios.index', compact('funcionarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $divisiones= Division::orderBy('nombre','ASC')->pluck('nombre','id');
        $departamentos= Departamento::orderBy('nombre','ASC')->pluck('nombre','id');
        return view('funcionarios.create', compact('divisiones', 'departamentos'));
                
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
            $destination='uploads/fotos';
            $imagen=$request->file('foto');
            $random= str_random(6);


        if (!empty($imagen)) {
            $filename    = $random . $imagen->getClientOriginalName(); // get the filename
            $imagen->move($destination, $filename); // move file to destination
        }else{
            $filename = '';
        }

        $user = [
            'name'     => $request->get('nombre'). ' '. $request->get('apellido'),
            'email'    => $request->get('email'),
            'password' => bcrypt($request->get('password')),
            'rol'      => 0,
                ];

        $user_save = User::create($user);
 
        $user_id = $user_save->id;
        
        $funcionario =    [
            'cedula'=>$request->get('cedula'),
            'nombre'=>$request->get('nombre'),
            'apellido'=>$request->get('apellido'),
            'nombre_completo' => $request->get('nombre').' '.$request->get('apellido'),
            'cargo'=>$request->get('cargo'),
            'division_id'=>$request->get('division_id'),
            'departamento_id'=>$request->get('departamento_id'),
            'telefono'=>$request->get('telefono'),
            'foto' => $filename,
            'user_id' => $user_id,
        ]; 


        $funcionario_save=Funcionario::create($funcionario);

        $message= $funcionario_save ? 'Registro Agregado Exitosamente' : 'Error Intente Nuevamente..';

        return redirect()->route('home')->with('message',$message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $funcionario = Funcionario::where('user_id', $id)->first();
      
        
         
        return view('funcionarios.show', compact('funcionario'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $funcionario= Funcionario::where('id', $id)->first();
        $divisiones= Division::orderBy('nombre','ASC')->pluck('nombre','id');
        $departamentos= Departamento::orderBy('nombre','ASC')->pluck('nombre','id');
        
        return view('funcionarios.edit', compact('funcionario','departamentos', 'divisiones'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $funcionario= Funcionario::where('id', $id)->first();
        
        $old_image = $funcionario->foto;

        $destination = 'uploads/fotos/';
        
        $image = $request->file('foto');
        
        $random = str_random(6);

        if (!empty($image)) {
            $filename    = $random . $image->getClientOriginalName(); // get the filename
            $image->move($destination, $filename); // move file to destination
            File::delete($destination . $old_image);
        }else{
            $filename = $old_image;
        }

        $data =
        [
            'cedula'=>$request->get('cedula'),
            'nombre'=>$request->get('nombre'),
            'apellido'=>$request->get('apellido'),
            'nombre_completo' => $request->get('nombre').' '.$request->get('apellido'),
            'cargo'=>$request->get('cargo'),
            'division_id'=>$request->get('division_id'),
            'departamento_id'=>$request->get('departamento_id'),
            'correo'=>$request->get('correo'),
            'telefono'=>$request->get('telefono'),
            'foto' => $filename
        ];
        

        $funcionario->fill($data);
        $updated = $funcionario->save();
        $message = $updated?'Registro actualizado correctamente.':'Error, intente nuevamente';
        return redirect()->route('funcionarios.index')->with('message', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $funcionario = Funcionario::where('id', $id)->first();
        $deleted = $funcionario->delete();
        $message = $deleted?'Registro eliminado correctamente' : 'Error al Eliminar';
        return redirect()->route('funcionarios.index')->with('message', $message);
        
    }
    public function divi($id)

    {

        $divisiones= Division::orderBy('nombre','ASC')->pluck('nombre','id');
        $departamentos= Departamento::orderBy('nombre','ASC')->pluck('nombre','id');
        return view('register', compact('departamentos', 'divisiones'));

    }

}
