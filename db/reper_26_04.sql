-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-04-2017 a las 14:38:37
-- Versión del servidor: 5.6.26
-- Versión de PHP: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `reper`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamentos`
--

CREATE TABLE IF NOT EXISTS `departamentos` (
  `id` int(10) unsigned NOT NULL,
  `division_id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `departamentos`
--

INSERT INTO `departamentos` (`id`, `division_id`, `nombre`, `descripcion`, `created_at`, `updated_at`) VALUES
(1, 1, 'Programación', '', '2017-03-17 17:11:48', '2017-03-17 17:11:48'),
(2, 1, 'Infraestructura', '', '2017-03-17 17:12:08', '2017-03-17 17:12:08'),
(3, 2, 'Inspección Regional', '', '2017-03-17 17:12:24', '2017-03-17 17:12:24'),
(4, 2, 'Contraloría Social', '', '2017-03-17 17:12:49', '2017-03-17 17:12:49'),
(5, 3, 'Organización y Doctrina', '', '2017-03-17 17:13:08', '2017-03-17 17:13:08'),
(6, 3, 'Control y Evaluación', '', '2017-03-17 17:13:30', '2017-03-17 17:13:30'),
(7, 4, 'No Aplica', '', '2017-03-17 17:13:46', '2017-03-17 17:13:46');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `divisions`
--

CREATE TABLE IF NOT EXISTS `divisions` (
  `id` int(10) unsigned NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `divisions`
--

INSERT INTO `divisions` (`id`, `nombre`, `descripcion`, `created_at`, `updated_at`) VALUES
(1, 'Tecnología de Información y Comunicación', 'DTIC', '2017-01-25 22:24:30', '2017-01-31 00:21:35'),
(2, 'Fiscalizacion', 'FISC', '2017-01-27 17:24:01', '2017-01-27 17:33:54'),
(3, 'División de Control Integral', 'DCI', '2017-01-26 16:47:40', '2017-01-31 00:31:29'),
(4, 'No Aplica', '', '2017-03-07 19:56:53', '2017-03-07 19:56:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `funcionarios`
--

CREATE TABLE IF NOT EXISTS `funcionarios` (
  `id` int(10) unsigned NOT NULL,
  `cedula` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apellido` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nombre_completo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cargo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `division_id` int(11) NOT NULL,
  `departamento_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `telefono` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `funcionarios`
--

INSERT INTO `funcionarios` (`id`, `cedula`, `nombre`, `apellido`, `nombre_completo`, `cargo`, `division_id`, `departamento_id`, `user_id`, `telefono`, `foto`, `created_at`, `updated_at`) VALUES
(1, 18895813, 'Pablo', 'Acosta', 'Pablo Acosta', 'Inspector de Gestión Regional', 1, 1, 1, '04243422707', '0m2rtRPenguins.jpg', '2017-03-15 18:29:34', '2017-03-15 18:29:34'),
(2, 20586901, 'CarlosLeonardo', 'Ledezma', 'CarlosLeonardo Ledezma', 'Inspector de Gestión Regional', 1, 1, 2, '04146578909', '2JkyOADesert.jpg', '2017-03-15 18:44:05', '2017-03-15 18:44:05'),
(3, 18804343, 'carlos', 'covis', 'carlos covis', 'JEFE ', 0, 1, 4, '1212121212', 'W4R105Lighthouse.jpg', '2017-03-15 19:03:27', '2017-03-15 19:03:27'),
(4, 18804345, 'carlos', 'Piña', 'carlos Piña', 'Inspector de Gestión Regional', 3, 6, 5, '04243422707', 'gxwqgHDesert.jpg', '2017-03-15 19:30:21', '2017-03-15 19:30:21'),
(5, 18804342, 'José', 'Piña', 'José Piña', 'Inspector de Gestión Regional', 3, 6, 6, '04243422707', '1UNoeODesert.jpg', '2017-03-15 19:31:26', '2017-03-15 19:31:26'),
(6, 1880436, 'Juan', 'Piña', 'Juan Piña', 'Inspector de Gestión Regional', 3, 6, 7, '04243422707', 'rDECuLDesert.jpg', '2017-03-15 19:32:12', '2017-03-15 19:32:12'),
(7, 23131231, 'Informatica', 'Acosta', 'Informatica Acosta', 'Inspector de Gestión Regional', 3, 6, 9, '04243422707', '', '2017-03-15 19:38:21', '2017-03-15 19:38:21'),
(8, 19472259, 'Miguel Alejandro', 'Pere Gonzalez', 'Miguel Alejandro Pérez González', 'Jefe de Division', 1, 7, 10, '04146578945', '', '2017-04-05 19:22:33', '2017-04-05 19:22:33');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_01_23_142703_create_tipos_table', 1),
(4, '2017_01_23_150619_create_permisos_table', 2),
(6, '2017_01_24_120832_create_funcionarios_table', 3),
(7, '2017_01_24_123130_create_divisions_table', 4),
(8, '2017_01_24_123429_create_departamentos_table', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos`
--

CREATE TABLE IF NOT EXISTS `permisos` (
  `id` int(10) unsigned NOT NULL,
  `funcionario_id` int(11) NOT NULL,
  `tipo_id` int(11) NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_final` date DEFAULT NULL,
  `cantidad_hora` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hora_inicio` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hora_final` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `observacion` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `img_scanner` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `permisos`
--

INSERT INTO `permisos` (`id`, `funcionario_id`, `tipo_id`, `fecha_inicio`, `fecha_final`, `cantidad_hora`, `hora_inicio`, `hora_final`, `observacion`, `status`, `img_scanner`, `created_at`, `updated_at`) VALUES
(1, 1, 5, '2017-03-15', '2017-03-15', '02:0', '9:00', '11:00', 'caminar', 0, '', '2017-03-15 19:53:04', '2017-03-15 19:53:06'),
(2, 1, 5, '2017-03-15', '2017-03-15', '2 Horas', '3:00', '5:00', 'diligencias personales', 0, '', '2017-03-15 23:59:09', '2017-03-15 23:59:11'),
(3, 1, 5, '2017-03-15', '2017-03-15', '2 Horas', '3:30', '5:00', 'diligencias personales', 0, '', '2017-03-16 00:04:07', '2017-03-16 22:53:46'),
(4, 1, 5, '2017-03-15', '2017-03-15', '2 Horas', '3:30', '5:00', 'diligencias personales', 0, '', '2017-03-16 00:05:06', '2017-03-16 22:53:46'),
(5, 2, 5, '2017-03-21', '2017-03-21', '', '', '', 'Ir a Caracas.', 0, '', '2017-03-21 22:51:19', '2017-03-28 16:25:04'),
(6, 4, 5, '2017-04-24', '2017-04-24', '1 Horas', '10:00', '11:20', 'diligencias personales', 0, '', '2017-04-24 19:15:29', '2017-04-24 22:23:10'),
(7, 4, 5, '2017-04-24', '2017-04-24', '', '', '', 'diligencias personales', 0, '', '2017-04-24 22:10:08', '2017-04-24 22:23:10'),
(8, 4, 5, '2017-04-24', '2017-04-24', '', '', '', 'diligencias personales y algo Mas.', 0, '', '2017-04-24 22:11:38', '2017-04-24 22:23:10'),
(9, 4, 5, '2017-04-24', '2017-04-24', '', '', '', 'diligencias personales', 0, '', '2017-04-24 22:18:15', '2017-04-24 22:23:10'),
(10, 7, 5, '2017-04-24', '2017-04-30', NULL, NULL, NULL, 'diligencias personales y algo Mas.', 1, '', '2017-04-24 22:24:58', '2017-04-24 22:28:59'),
(11, 8, 5, '2017-04-24', '2017-04-30', '', '', '', 'diligencias ', 1, '', '2017-04-24 22:34:48', '2017-04-24 22:34:48'),
(12, 8, 5, '2017-04-24', '2017-04-24', '0 Horas', '2:35', '3:35', '', 0, '', '2017-04-24 22:36:30', '2017-04-24 22:37:05'),
(13, 8, 5, '2017-04-24', '2017-04-24', '1 Horas', '2:35', '3:35', 'diligencias personales y algo Mas.', 0, '', '2017-04-24 22:36:49', '2017-04-24 22:37:05'),
(14, 2, 5, '2017-04-24', '2017-04-28', '', '', '', 'diligencias personales en el Banco', 1, '', '2017-04-25 00:08:35', '2017-04-25 00:08:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos`
--

CREATE TABLE IF NOT EXISTS `tipos` (
  `id` int(10) unsigned NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `tipos`
--

INSERT INTO `tipos` (`id`, `nombre`, `descripcion`, `created_at`, `updated_at`) VALUES
(5, 'OBLIGATORIO', '', '2017-01-27 22:53:20', '2017-02-21 19:18:03'),
(6, 'POTESTATIVO', '', '2017-01-27 22:56:27', '2017-02-21 19:18:18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rol` tinyint(1) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `rol`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Pablo Acosta', 'acostapab@gmail.com', '$2y$10$AEiLTwApu3B0uinsgaQAx.wB3AXJdMy1iZ2tChDNWYt7XAgjRGpd6', 1, 'aQYcfTCkLjZAQddNPR67f2BbYJ7Mxzq8g7R3va5jbITn2WAL61QUUpQ2wfM8', '2017-03-15 18:29:34', '2017-04-24 19:13:08'),
(2, 'CarlosLeonardo Ledezma', 'carlosjcovis@gmail.com', '$2y$10$Fhlz8gygsU9ZC0zSUwAKfuIipzugI3h0Gp0KmTS1S7jB756JsMr06', 0, NULL, '2017-03-15 18:44:05', '2017-03-15 18:44:05'),
(4, 'carlos covis', 'carlossiglo@gmail.com', '$2y$10$6Cv7iWpN/rI3MvXPXdXudeKH/J5CDhTRXnJyU4.92SVyhmEnv8AhS', 0, 'MKiQ5bU8PMd7ZBl1UKEeSZ7nJGzzc1vrFlshP7SgMhKQqDaPElbhT6z4Aaxv', '2017-03-15 19:03:27', '2017-03-15 19:14:13'),
(5, 'carlos Piña', 'carlos@gmail.com', '$2y$10$.GmRp81.bZR/SzdnfVpar.LxfplVoq3e6jOheO2q7xga84ma./7AW', 0, '8zF08lnuUxFKFF7zAiTIYcwoDkfvG5wybie9hFZSQElpc7uWdQwi8zVHRMSo', '2017-03-15 19:30:21', '2017-04-24 22:22:48'),
(6, 'José Piña', 'jose@gmail.com', '$2y$10$JzfQNK8vLsHPMjcey6o8xehOkb3vkjP8y96mwfLKQl09ZLIWr4Km2', 0, NULL, '2017-03-15 19:31:26', '2017-03-15 19:31:26'),
(7, 'Juan Piña', 'juan@gmail.com', '$2y$10$sOYRAW0vrmoZfbL6I6U3LefFLbltxZQdpdk5Na.HeosrHlZIchGym', 0, NULL, '2017-03-15 19:32:12', '2017-03-15 19:32:12'),
(9, 'Informatica Acosta', 'informatica@gmail.com', '$2y$10$.I3/7R6hUGRNuSmXJh2Ip.y9mVwmOQMoiKXhjTHcWDFKlzyG2qJkq', 0, 'iXSqaqpk4lLlKzvkkKFJC6fKgs4NnXkxa8He1tIj4Qkk5q9rE5yn4uKGY93R', '2017-03-15 19:38:21', '2017-03-15 19:42:06'),
(10, 'Miguel Alejandro Pérez González', 'kmatagua@gmail.com', '$2y$10$X/z1if.nYriBlLjSroUy7Oav06JbZsxfc24PFe3.ZVKXlJIpFxj06', 0, 'fLGpkDNCpSGwUfhLdGnaP7wUvfGbHuzZqY1rQaK2mGQlO9h6OucAs0aVyZyb', '2017-04-05 19:22:33', '2017-04-05 19:22:55');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `divisions`
--
ALTER TABLE `divisions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `funcionarios`
--
ALTER TABLE `funcionarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indices de la tabla `permisos`
--
ALTER TABLE `permisos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipos`
--
ALTER TABLE `tipos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `divisions`
--
ALTER TABLE `divisions`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `funcionarios`
--
ALTER TABLE `funcionarios`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `permisos`
--
ALTER TABLE `permisos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `tipos`
--
ALTER TABLE `tipos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
