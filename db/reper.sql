-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-02-2017 a las 16:35:22
-- Versión del servidor: 5.6.26
-- Versión de PHP: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `reper`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamentos`
--

CREATE TABLE IF NOT EXISTS `departamentos` (
  `id` int(10) unsigned NOT NULL,
  `division_id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `departamentos`
--

INSERT INTO `departamentos` (`id`, `division_id`, `nombre`, `descripcion`, `created_at`, `updated_at`) VALUES
(1, 3, 'Organización y Doctrina', '', '2017-01-26 23:14:08', '2017-02-14 22:27:33'),
(2, 2, 'Inspección Regional', '', '2017-01-27 00:19:07', '2017-02-14 22:28:38'),
(4, 1, 'Programación', '', '2017-01-27 17:40:17', '2017-02-14 22:33:49'),
(5, 2, 'Controlaría Social', '', '2017-02-14 22:25:20', '2017-02-14 22:25:20'),
(6, 3, 'Control y Evaluación', '', '2017-02-14 22:29:29', '2017-02-14 22:29:29'),
(7, 1, 'Infraestructura', '', '2017-02-14 22:30:31', '2017-02-14 22:30:31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `divisions`
--

CREATE TABLE IF NOT EXISTS `divisions` (
  `id` int(10) unsigned NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `divisions`
--

INSERT INTO `divisions` (`id`, `nombre`, `descripcion`, `created_at`, `updated_at`) VALUES
(1, 'Tecnología de Información y Comunicación', 'Informática', '2017-01-25 22:24:30', '2017-01-31 00:21:35'),
(2, 'Fiscalizacion', 'Validación', '2017-01-27 17:24:01', '2017-01-27 17:33:54'),
(3, 'División de Control Integral', 'Seguimiento', '2017-01-26 16:47:40', '2017-01-31 00:31:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `funcionarios`
--

CREATE TABLE IF NOT EXISTS `funcionarios` (
  `id` int(10) unsigned NOT NULL,
  `cedula` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apellido` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nombre_completo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cargo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `division_id` int(11) NOT NULL,
  `departamento_id` int(11) NOT NULL,
  `correo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `funcionarios`
--

INSERT INTO `funcionarios` (`id`, `cedula`, `nombre`, `apellido`, `nombre_completo`, `cargo`, `division_id`, `departamento_id`, `correo`, `telefono`, `foto`, `created_at`, `updated_at`) VALUES
(3, 18895813, 'Pablo', 'Acosta', 'Pablo Acosta', 'Inspector de Gestión Regional', 1, 4, 'acostapab@gmail.com', '04243422707', '4CJaBSrsz_foto_carnet.png', '2017-01-27 22:31:35', '2017-02-16 21:33:36'),
(4, 18834820, 'Nestor', 'Montero', 'Nestor Montero', 'Jefe de Departamento', 1, 2, 'montero.nestor@gmail.com', '04243555776', 'q0qwKjmercedes_benz_rolls_royce_.jpg', '2017-01-30 16:42:44', '2017-02-06 19:19:34'),
(5, 19988776, 'CarlosLeonardo', 'Ledezma', 'CarlosLeonardo Ledezma', 'Inspector de Gestión', 1, 4, 'esemesmo@gmail.com', '01234567890', 'GFAtA1mercedes_benz_rolls_royce_.jpg', '2017-01-31 16:39:00', '2017-02-02 17:01:30'),
(6, 17675899, 'Liseth', 'Piña', 'Liseth Piña', 'Inspector de Gestión', 1, 4, 'alguien@gmail.com', '04243555776', 'DLIIr3wallpaper.jpg', '2017-01-31 17:50:36', '2017-01-31 19:49:24'),
(7, 19472259, 'Miguel Alejandro', 'Perez Gonzalez', 'Miguel Alejandro Perez Gonzalez', 'Jefe de Division', 1, 1, 'camatagua89@gmail.com', '04146578945', 'FlpsuMwallpaper.jpg', '2017-02-06 18:38:00', '2017-02-06 18:41:55');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_01_23_142703_create_tipos_table', 1),
(4, '2017_01_23_150619_create_permisos_table', 2),
(6, '2017_01_24_120832_create_funcionarios_table', 3),
(7, '2017_01_24_123130_create_divisions_table', 4),
(8, '2017_01_24_123429_create_departamentos_table', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos`
--

CREATE TABLE IF NOT EXISTS `permisos` (
  `id` int(10) unsigned NOT NULL,
  `funcionario_id` int(11) NOT NULL,
  `tipo_id` int(11) NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_final` date NOT NULL,
  `observacion` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `img_scanner` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `permisos`
--

INSERT INTO `permisos` (`id`, `funcionario_id`, `tipo_id`, `fecha_inicio`, `fecha_final`, `observacion`, `status`, `img_scanner`, `created_at`, `updated_at`) VALUES
(12, 5, 5, '2017-02-13', '2017-02-14', 'diligencias personales', 0, '7pMhwNKoala.jpg', '2017-02-03 18:07:17', '2017-02-22 17:19:59'),
(14, 3, 5, '2017-02-06', '2017-02-08', 'diligencias personales', 0, 'Xtu3lJPenguins.jpg', '2017-02-06 19:59:09', '2017-02-22 17:19:59'),
(16, 5, 5, '2017-02-06', '2017-02-05', 'eso memso 4', 0, 'O9HlRIKoala.jpg', '2017-02-07 00:29:11', '2017-02-22 17:19:59'),
(17, 6, 5, '2017-02-20', '2017-02-21', 'enferma de la rodilla', 0, 'XcllhYLighthouse.jpg', '2017-02-20 18:53:06', '2017-02-22 17:19:59'),
(18, 6, 5, '2017-02-22', '2017-02-23', 'no se sabe', 1, 'hnD0j0area interna.png', '2017-02-22 17:31:59', '2017-02-22 17:31:59'),
(19, 4, 5, '2017-02-22', '2017-02-20', 'hu', 0, '', '2017-02-22 17:33:23', '2017-02-22 17:33:24'),
(20, 5, 5, '2017-02-22', '2017-02-22', 'joj', 0, '', '2017-02-22 17:33:52', '2017-02-22 17:33:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos`
--

CREATE TABLE IF NOT EXISTS `tipos` (
  `id` int(10) unsigned NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `tipos`
--

INSERT INTO `tipos` (`id`, `nombre`, `descripcion`, `created_at`, `updated_at`) VALUES
(5, 'OBLIGATORIO', '', '2017-01-27 22:53:20', '2017-02-21 19:18:03'),
(6, 'POTESTATIVO', '', '2017-01-27 22:56:27', '2017-02-21 19:18:18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Pablo', 'acostapab@gmail.com', '$2y$10$2MpMek6op/G5630905Y3g.aSRzP9q53LgJPcdJrqmoJ7QtL6lzRfy', 'g3fvRhiRSgiSkruDivw4uCbHlTJWsLzziCkH9vSDVpjxKcYhYoh5v0pdu8ev', '2017-02-03 16:48:15', '2017-02-22 18:02:11');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `divisions`
--
ALTER TABLE `divisions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `funcionarios`
--
ALTER TABLE `funcionarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indices de la tabla `permisos`
--
ALTER TABLE `permisos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipos`
--
ALTER TABLE `tipos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `divisions`
--
ALTER TABLE `divisions`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `funcionarios`
--
ALTER TABLE `funcionarios`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `permisos`
--
ALTER TABLE `permisos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT de la tabla `tipos`
--
ALTER TABLE `tipos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
