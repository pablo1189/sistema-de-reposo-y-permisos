<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('tipos', 'TipoController');

Route::resource('permisos', 'PermisoController');

Route::resource('departamentos', 'DepartamentoController');

Route::resource('funcionarios', 'FuncionarioController');

Route::resource('divisions','DivisionController');

Auth::routes();

#Para hacer los Selects dependientes
Route::get('consulta/departamentos/{id}', 'PermisoController@getDepartamentos');

Route::get('consulta/funcionarios/{id}', 'PermisoController@getFuncionarios');


Route::get('change', 'PermisoController@changeStatus');

#Route::get('/home', 'HomeController@index');

Route::get('/home', [
		'uses' => 'HomeController@index',
		'as'   => 'home'
		 ]);



Route::get('permisos/buscar', [
		'uses' => 'PermisoController@buscar',
		'as'   => 'permisos.buscar'
		 ]);

#Route::get('permisos/buscar', 'PermisoController@buscar');

Route::get('resultado', [
		'uses' => 'PermisoController@resultado',
		'as'   => 'resultado'
		  ]);

Route::get('historial', [
		'uses' => 'PermisoController@historial',
		'as'   => 'historial'
		  ]);


Route::get('permisos/pdf/{id}',
	[
		'uses' => 'PermisoController@pdf',
		'as' => 'permisos.pdf'
	]
	);

Route::get('permisos/pdf2/{id}',
	[
		'uses' => 'PermisoController@pdf2',
		'as' => 'permisos.pdf2'
	]
	);


Route::get('pdf3', 'PermisoController@pdf3');

Route::get('pdf4', 'PermisoController@pdf4');

//Route::get('consulta', 'PermisoController@consulta');

Route::get('consulta', 'PermisoController@consulta');

Route::get('historial', 'PermisoController@historial');


Route::group(['middleware'=>['rol']], function(){

Route::get('rol',function(){
	return('/');
	});
});

