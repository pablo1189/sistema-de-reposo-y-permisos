<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermisosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permisos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('funcionario_id');
            $table->integer('tipo_id');
            $table->date('fecha_inicio');
            $table->date('fecha_final');
            $table->string('cantidad_hora');
            $table->string('hora_inicio');
            $table->string('hora_final');
            $table->text('observacion');
            $table->boolean('status');
            $table->string('img_scanner');
           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permisos');
    }
}
