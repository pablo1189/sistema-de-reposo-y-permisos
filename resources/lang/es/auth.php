<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Estas credenciales No Coinciden Con Nuestros Registros.',
    'throttle' => 'Demasiados intentos de Inicio de Sesión. Vuelva a Intentarlo en 2 minutos.',

];
