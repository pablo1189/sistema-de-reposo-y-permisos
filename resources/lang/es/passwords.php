<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Las contraseñas Deben Tener Al Menos Seis Caracteres y Coincidir con la Confirmación.',
    'reset' => 'Tu contraseña Ha Sido Restablecida!',
    'sent' => '¡Hemos Enviado su Enlace de Restablecimiento de Contraseña Por Correo Electrónico!',
    'token' => 'This password reset token is invalid.',
    'user' => "No se Ha encontrado un Usuario con esa Dirección de Correo Electrónico.",

];
