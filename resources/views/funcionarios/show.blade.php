@extends('layouts.template')

@section('title')
        SIREP | DGCSGG
@endsection

@section('content')


  <div class="container">
  
  <div class="col-md-8 col-md-offset-2">
    
     <div class="panel panel-info">
     <div class="panel-heading">

      <h2 class="title" style="color: blue">Funcionarios.</h2>
    
    <img src="{{asset('uploads/fotos/'.$funcionario->foto)}}" alt="" width="100px" height="120px" class="img-thumbnail img-circle">
    
   
    </div>
    <div class="panel-body">

    <table class="table table-striped table-bordered table-hover">

          <tr>
            <th>Cedula</th>
            <td> {{$funcionario->cedula}}</td>
          </tr>
          
            <tr>
            <th>Nombre y Apellido</th>
            <td> {{$funcionario->nombre_completo}}</td>
          </tr>

           <tr>
            <th>Cargo</th>
            <td> {{$funcionario->cargo}}</td>
          </tr>

           <tr>
            <th>Departamento</th>
            <td> {{$funcionario->departamento->nombre}}</td>
          </tr>
          
            <tr>
            <th>Division</th>
            <td> {{$funcionario->division->nombre}}</td>
          </tr>

          <tr>
            <th> Correo</th>
            <td>{{$funcionario->correo}}</td>
          </tr>
          
          <tr>
            <th> Teléfono </th>
            <td>{{$funcionario->telefono}}</td>
          </tr>

          </table>


    

     </div> 
     </div>
     </div>
</div> 
@endsection
  