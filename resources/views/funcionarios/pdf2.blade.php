	<style type="text/css">

@page {
        margin-top: 2em;
        margin-left: 2em;
}


#mitabla{
/*estilo de el cuerpo de nuestra tabla*/
background-color: black;
width:100%;
margin:24px auto;
border-radius: 5px 2px 5px 2px;

font-family:helvetica,arial,sans-serif;
font-weight:normal;
font-size: 10px;
text-transform: uppercase;
}

#mitabla th{
/*estilo de los elementos(th)*/
background-color: #B0C4DE;
color: black;
font-weight: bold;
font-size:10px;
/*padding: 8px;*/
}

#mitabla td{

/*estilo a los elementos (td)*/
/*width:100px;
height: 24px;*/
color: #000;
margin: 15px;
padding: 8px;
}

/*Aqui esta la magia ahora vamos usar el  nth-child()**/

#mitabla tr:nth-child(odd){

/*para los colores de las filas impares*/
background-color: #ffffff;
}

#mitabla tr:nth-child(even){

/*para los colores de las filas pares*/
background:#EDEDED;
}

#header {
 margin-bottom: 1em; margin-top: 0em;}

#footer { position: fixed;}

#contenido{
	margin-top: 1em;
}
h2{
	font-family:helvetica,arial,sans-serif;
	font-weight: bold;
	font-size: 16px;
}

p {
	font-family:helvetica,arial,sans-serif;
	font-size: 10px;
}


#mitabla1{
/*estilo de el cuerpo de nuestra tabla*/
background-color: black;
width:40%;
margin:24px auto;
border-left: 1px solid #ccc;
border-right: 1px solid #ccc;
font-family:helvetica,arial,sans-serif;
font-weight:normal;
font-size: 10px;
text-transform: uppercase;
}

#mitabla1 th{
/*estilo de los elementos(th)*/
background-color: black;
color: #fff;
font-weight: bold;
font-size:10px;
/*padding: 8px;*/
}

#mitabla1 td{

/*estilo a los elementos (td)*/
/*width:100px;
height: 24px;*/
color: #000;
margin: 15px;
padding: 8px;
}

/*Aqui esta la magia ahora vamos usar el  nth-child()**/

#mitabla1 tr:nth-child(odd){

/*para los colores de las filas impares*/
background-color: #ffffff;
}

#mitabla1 tr:nth-child(even){

/*para los colores de las filas pares*/
background:#EDEDED;
}


</style>

	
	<div>
  <img src="C:/xampp/htdocs/reper/public/img/dgcsgggebg1.png" align="left" width="200px" height="100px">
  

  <img src="C:/xampp/htdocs/reper/public/img/logo.png" align="right" width="200px" height="100px">
 
<br> <br><br><br><br><br>

  <hr>
  
  
 <table align="center">

  <tr>
    <td><h2>Sistema De Reposos y Permisos   (SIREP)</h2></td>
    <td> <h3>Historial de Permisos.</h3></td>

  </tr>
   
  </table>
		
	<div class="panel-body">
      
    <table  id="mitabla" align="center">
      <thead>
          <tr>
              <th> Funcionario</th>
              <th> Tipo de Permiso/Reposo</th>
              <th> Cargo </th>
              <th> Departamento </th>
              <th> Division </th>
              <th>Fecha Inicio</th>
              <th>Fecha Limite</th>
              <th>Motivo de la Solicitud</th>
              <th>Status</th>
          </tr>
      </thead>   
    
      <tbody> 

    @foreach($permiso as $perm)
      <tr>
                              
      <td>{{ $perm->funcionario->nombre_completo }}</td>
      <td>{{ $perm->tipo->nombre }}</td>
      <td>{{$perm->funcionario->cargo}}</td>
      <td>{{$perm->funcionario->departamento->nombre}}</td>
      <td>{{$perm->funcionario->division->nombre}}</td>
      <td> {{date("d-m-Y",strtotime($perm->fecha_inicio))}}</td>
      <td> {{date("d-m-Y",strtotime($perm->fecha_final))}}</td> 
      <td>{{$perm->observacion}} </td>      
      <td>@if ($perm->fecha_inicio >= $perm->fecha_final)
              {{'Finalizado'}}
            @else
              {{'Activa'}}
            @endif
      </td>                                 
      </tr>  
     @endforeach
  </tbody>

  </table>
    

  </div> 
  <div align="center">
  <i>¡Guárico... Revolución Irreversible!</i>
  <hr>
  <p>Dirección de Control y Seguimiento de Gestión de Gobierno <br>
  Calle Roscio c/c Calle Rivas, Edf. Battista, PB <br>
  Telf. 0246-4315881</p>
  </div>