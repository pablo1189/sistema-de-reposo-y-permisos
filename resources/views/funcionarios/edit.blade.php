@extends('layouts.template')
@section('title')
       Funcionarios | Editar
@endsection

@section('content')
<div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <h3>Editar Funcionario: {{$funcionario->nombre }}</h3>
                @if(count($errors)>0)
                <div class="alert alert-danger">
                        <ul>
                                @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                @endforeach
                        </ul>
                </div>
                @endif
        </div>
</div>

{!! Form::model($funcionario, ['method' => 'PUT', 'route' =>['funcionarios.update', $funcionario->id], 'enctype' => 'multipart/form-data']) !!}

{{ Form::token()}}

<div class="col-md-8 col-md-offset-2">
    <h3>Funcionarios</h3>

<div class="form-group">
{{  Form::label('Cedula:') }}
    {!! Form::text('cedula', null, ['class' => 'form-control','placeholder' => 'Cedula']) !!}
</div>

<div class="form-group">
{{  Form::label('Nombre:') }}
    {!! Form::text('nombre', null, ['class' => 'form-control','placeholder' => 'Nombre']) !!}
</div>

<div class="form-group">
{{  Form::label('Apellido:') }}
    {!! Form::text('apellido', null, ['class' => 'form-control','placeholder' => 'Apellido']) !!}
</div>

<div class="form-group">
{{  Form::label('Cargo:') }}
    {!! Form::text('cargo', null, ['class' => 'form-control','placeholder' => 'Cargo']) !!}
</div>




<div class="form-group">
{{  Form::label('División:') }}
  {!! Form::select('division_id', $divisiones, null, ['class' =>'form-control']) !!}
</div>

<div class="form-group">
{{  Form::label('Departamento:') }}
  {!! Form::select('departamento_id', $departamentos, null, ['class' =>'form-control', 'placeholder' => '-- Seleccione --', ]) !!}
</div>
   
<div class="form-group">
{{  Form::label('Correo:') }}
    {!! Form::text('correo', null, ['class' => 'form-control','placeholder' => 'Correo'])!!}
</div>

<div class="form-group">
{{  Form::label('Teléfono:') }}
    {!! Form::text('telefono', null, ['class' => 'form-control','placeholder' => 'Teléfono..'])!!}
</div>

<div class="form-group">
{{  Form::label('Foto') }}

    {!! Form::file('foto', null, ['class'=>'form-control','placeholder' => 'Foto...']) !!}
    <br>
    </div>

    {!! Form::submit('Guardar', array('class' => 'btn btn-success')) !!}

{!! Form::close() !!}

@endsection
