@extends('layouts.template')
@section('title')
        SIREP | Departamentos
@endsection
@section('content')
<div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <h3>Listado de Departamentos<a href="{{asset('departamentos/create')}}" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Nuevo" data-original-title="Tooltip on top"><i class="fa fa-plus" aria-hidden="true"></i></a></h3>
                
        </div>
</div>
<div class="row">
        <div class="col-md-12">
                <div class="table-responsive">
                        <table id="tabla" class="display" cellspacing="0" width="100%"  class="table table-striped table-condensed table-bordered table-hover" data-form="deleteForm">
                                <thead>
                                        
                                        <th>Division</th>
                                        <th>Nombre</th>
                                        
                                        <th width="176px"><p class="text-center"> Acción</p></th>
                                </thead>

                                
                                @foreach($departamentos as $departs)
                                        <tr>
                                               
                                                <td>{{ $departs->division->nombre }}</td>
                                                <td>{{ $departs->nombre }}</td>
                                              
                                                <td>
                                                    
                                                    <div class="row">
                                                    <div class="col-md-4">
                                                    <a class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Ver" data-original-title="Tooltip on top" href="{{ route('departamentos.show', $departs->id) }}">
                                                        <i class="fa fa-eye"></i></a>
                                                    </div>

                                                        <div class="col-md-4 ">
                                                            <a href="{{ URL::action('DepartamentoController@edit', $departs->id)}}" class="btn btn-info" data-toggle="tooltip" data-placement="top" data-original-title="Editar" ><i class="fa fa-edit"></i></a>
                                                        </div>

                                                        <div class="col-md-4">
                                                        {!! Form::model($departs, ['method' => 'delete', 'route' => ['departamentos.destroy', $departs->id], 'class' =>'form-inline form-delete']) !!}
                                                            {!! Form::hidden('id', $departs->id) !!}
                                                            <button type="submit" name="delete_modal" class="btn btn-danger delete" data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar"><i class="fa fa-trash"></i></button>
                                                        {!! Form::close() !!}
                                                        </div>
                                                    </div>
                                                        
                                                        
                                                </td>
                                        </tr>
                                        
                                @endforeach
                        </table>
                </div>
                
        </div>
</div>
@endsection