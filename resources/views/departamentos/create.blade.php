@extends('layouts.template')

@section('content')


	{{ Form::open(['action' => 'DepartamentoController@store']) }}


	{!! csrf_field() !!}


         {{  Form::label('División') }}

          {!! Form::select('division_id', $divisiones, null, ['class'=>'form-control', 'placeholder' => '-- Seleccione --', 'required' ]) !!}  

          {{  Form::label('Nombre') }}
                      
          {{  Form::text ('nombre', null, ['class'=>'form-control', 'placeholder' => 'Nombre del Departamento', 'required'])}}

          {{  Form::label('Descripción') }}

          {{  Form::text('descripcion', null, ['class' => 'form-control', 'placeholder' => 'Descripción']) }}   
         
          {!!  Form::submit('Guardar', array('class' => 'btn btn-success')) !!}
        
    
		{{ Form::close()}}


@endsection