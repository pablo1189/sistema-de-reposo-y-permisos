@extends('layouts.template')
@section('title')
       Departamentos  | Editar
@endsection

@section('content')
<div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <h3>Editar Departamento: {{ $departamento->nombre }}</h3>
                @if(count($errors)>0)
                <div class="alert alert-danger">
                        <ul>
                                @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                @endforeach
                        </ul>
                </div>
                @endif
        </div>
</div>



{!! Form::model($departamento, ['method' => 'PUT', 'route' =>['departamentos.update', $departamento->id]]) !!}

{{ Form::token()}}

<div class="col-md-8 col-md-offset-2">
    <h3>Departamento</h3>

<div class="form-group">
{{  Form::label('División:') }}
  {!! Form::select('division_id', $division, null, ['class' =>'form-control']) !!}
</div>

    
<div class="form-group">
{{  Form::label('Nombre:') }}
    {!! Form::text('nombre', null, ['class' => 'form-control','placeholder' => 'Nombre']) !!}
</div>
    
<div class="form-group">
{{  Form::label('Descripción:') }}
    {!! Form::text('descripcion', null, ['class' => 'form-control','placeholder' => 'Descripción'])!!}
</div>


    {!! Form::submit('Guardar', array('class' => 'btn btn-success')) !!}

{!! Form::close() !!}

@endsection
