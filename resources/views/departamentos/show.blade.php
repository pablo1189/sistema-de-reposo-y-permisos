@extends('layouts.template')

@section('title')
        SIREP | DCSGG
@endsection

@section('content')


  <div class="container">
  
  <div class="col-md-8 col-md-offset-2">
    
     <div class="panel panel-default">
     <div class="panel-heading">

      <h2 class="title" style="color: blue">Departamentos.</h2>
    
    
   
    </div>
    <div class="panel-body">

    <table class="table table-striped table-bordered table-hover">

          <tr>
            <th>Division</th>
            <td> {{$departamento->division->nombre}}</td>
          </tr>
          
            <tr>
            <th>Nombre</th>
            <td> {{$departamento->nombre}}</td>
          </tr>

           <tr>
            <th>Descripción</th>
            <td> {{$departamento->descripcion}}</td>
          </tr>

    </table>


    

     </div> 
     </div>
     </div>
  </div> 

  </div>

   
</div>



@endsection
  