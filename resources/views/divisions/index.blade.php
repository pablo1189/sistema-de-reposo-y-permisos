@extends('layouts.template')
@section('title')
       SIREP | Divisiones
@endsection
@section('content')
<div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <h3>Listado de Divisiones<a href="{{asset('divisions/create')}}" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Nuevo" data-original-title="Tooltip on top"><i class="fa fa-plus" aria-hidden="true"></i></a></h3>
                
        </div>
</div>
<div class="row">
        <div class="col-md-12">
                <div class="table-responsive">
                        <table id="tabla" class="display" cellspacing="0" width="100%"  class="table table-striped table-condensed table-bordered table-hover" data-form="deleteForm">
                                <thead>
                                        
                                        <th>Nombre</th>
                                        <th>Descripción</th>
                                        <th><p class="text-center"> Acción</p></th>
                                </thead>
                                @foreach($divisions as $divs)
                                        <tr>
                                                
                                                <td>{{ $divs->nombre }}</td>
                                                <td>{{ $divs->descripcion }}</td>
                                                <td>
                                                    <div class="row">
                                                    
                                                        <div class="col-md-6">
                                                            <a href="{{ URL::action('DivisionController@edit', $divs->id)}}" class="btn btn-info" data-toggle="tooltip" data-original-title="Tooltip on top" data-placement="top" title="Editar"><i class="fa fa-edit"></i></a>
                                                        </div>

                                                        <div class="col-md-6">
                                                        {!! Form::model($divs, ['method' => 'delete', 'route' => ['divisions.destroy', $divs->id], 'class' =>'form-inline form-delete']) !!}
                                                            {!! Form::hidden('id', $divs->id) !!}
                                                            <button type="submit" name="delete_modal" class="btn btn-danger delete" data-toggle="tooltip" data-placement="top" title="Eliminar" data-original-title="Eliminar"><i class="fa fa-trash"></i></button>
                                                        {!! Form::close() !!}
                                                        </div>
                                                    </div>
                                                        
                                                        
                                                </td>
                                        </tr>
                                        
                                @endforeach
                        </table>
                </div>
                
        </div>
</div>
@endsection






