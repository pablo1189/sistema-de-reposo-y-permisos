@extends('layouts.template')

@section('title')
        Divisiones | Agregar
@endsection

@section('content')
<div class="col-md-8 col-md-offset-2">
	<h3>Divisiones</h3>
{!! Form::open(['action' => 'DivisionController@store']) !!}

	{!! Form::token() !!}
<div class="form-group">
	{!! Form::text('nombre', null, ['class' => 'form-control','placeholder' => 'Nombre']) !!}
</div>
	
<div class="form-group">
	{!! Form::text('descripcion', null, ['class' => 'form-control','placeholder' => 'Descripción'])!!}
</div>

	
	{!! Form::submit('Guardar', array('class' => 'btn btn-success')) !!}


{!! Form::close() !!}
</div>


@endsection