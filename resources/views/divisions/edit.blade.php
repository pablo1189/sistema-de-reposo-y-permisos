@extends('layouts.template')
@section('title')
        Editar | Division
@section('content')
<div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <h3>Editar Permiso: {{ $division->nombre }}</h3>
                @if(count($errors)>0)
                <div class="alert alert-danger">
                        <ul>
                                @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                @endforeach
                        </ul>
                </div>
                @endif
        </div>
</div>

{!! Form::model($division, ['method' => 'PUT', 'route' =>['divisions.update', $division->id]]) !!}
{{ Form::token()}}
<div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text" name="nombre" required value="{{$division->nombre}}" class="form-control">
                </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                        <label for="direccion">Descripcion</label>
                        <input type="text" name="descripcion" value="{{$division->descripcion}}" class="form-control" placeholder="Descripcion...">
                </div>
        </div>
        
      
               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                                <button type="submit" class="btn btn-primary">Guardar</button>
                                <button type="reset" class="btn btn-danger">Cancelar</button>
                        </div>
                </div>
                
</div>
{!! Form::close() !!}

@endsection
