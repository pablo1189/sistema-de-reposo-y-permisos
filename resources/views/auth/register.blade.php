@extends('layouts.template')

@section('content')
<div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-primary" >
            <div class="panel-heading" align="center"><h3><i class="fa fa-users fa-2x"> </i> Funcionarios</h3></div>
<div class="panel-body">
{!! Form::open(['action' => 'FuncionarioController@store', 'enctype'=>'multipart/form-data']) !!}
{!!csrf_field()!!} 
    {!! Form::token() !!}

    {{  Form::label('Cedula') }}

    {!! Form::text('cedula', null, ['class'=>'form-control','placeholder' => 'Cedula..']) !!}

    {{  Form::label('Nombre') }}

    {!! Form::text('nombre', null, ['class'=>'form-control','placeholder' => 'Nombre...']) !!}

    {{  Form::label('Apellido') }}

    {!! Form::text('apellido', null, ['class'=>'form-control','placeholder' => 'Apellido..']) !!}

    {{  Form::label('Cargo') }}

    {!! Form::text('cargo', null, ['class'=>'form-control','placeholder' => 'Cargo..']) !!}

    {{  Form::label('División') }}
    
    {!! Form::select('division_id', $divisiones, null, ['class'=>'form-control', 'placeholder' => '-- Seleccione --', 'required' ]) !!}
    
    {{  Form::label('Departamento') }}  
   
    {!! Form::select('departamento_id',$departamentos , null, ['class'=>'form-control', 'placeholder' => '-- Seleccione --', ]) !!}

    {{  Form::label('Teléfono') }}

    {!! Form::text('telefono', null, ['class'=>'form-control','placeholder' => 'Telefono...']) !!}

    {{  Form::label('Foto') }}

    {!! Form::file('foto', null, ['class'=>'form-control','placeholder' => 'Foto...']) !!}


    <br>


    
  
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <div class="col-md-3">
                                    <input class="form-control" id="email" name="email" placeholder="Correo" type="email" value="{{ old('email') }}">
                                        @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>
                                                {{ $errors->first('email') }}
                                            </strong>
                                        </span>
                                        @endif
                                    </input>
                                </div>

                                                         
                                <div class="row form-group">
                                
                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <div class="col-md-3">
                                            <input class="form-control" id="password" name="password" placeholder="Clave" type="password"> 
                                                @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>
                                                        {{ $errors->first('password') }}
                                                    </strong>
                                                </span>
                                                @endif
                                            </input>
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                        <div class="col-md-3">
                                            <input class="form-control" id="password-confirm" name="password_confirmation" placeholder="Confirmar Clave" type="password">
                                                @if ($errors->has('password_confirmation'))
                                                <span class="help-block">
                                                    <strong>
                                                        {{ $errors->first('password_confirmation') }}
                                                    </strong>
                                                </span>
                                                @endif
                                            </input>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        {!! Form::submit('Guardar', array('class' => 'btn btn-success')) !!}
                    </div>    
                </div>
            </div>
            
        </div>

    </div>




    {{ Form::close()}}
@endsection
 </div>


<script>
    $(":file").filestyle();     
</script>
