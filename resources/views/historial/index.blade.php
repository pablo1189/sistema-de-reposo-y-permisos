@extends('layouts.template')
@section('title')
        Historial
@endsection
@section('content')
<div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <h3>Listado de Funcionarios</h3>
                
        </div>
</div>
<div class="row">
        <div class="col-md-12">
                <div class="table-responsive">
                        <table id="tabla" class="display" cellspacing="0" width="100%"  class="table table-striped table-condensed table-bordered table-hover" data-form="deleteForm">
                                <thead>
                                    
                                        
                                        <th>Nombre</th>
                                        <th>Apellido</th>
                                        <th>Cargo</th>                                      
                                        
                                        <th width="176px"><p class="text-center"> Acción</p></th>
                                </thead>
                                @foreach($funcionarios as $func)
                                        <tr>
                                                
                                                
                                                <td>{{ $func->nombre }}</td>
                                                <td>{{ $func->apellido }}</td>
                                                <td>{{ $func->cargo }}</td>                                              
                                                
                                                <td>
                                                    <div class="row">
                                                    <div class="col-md-4">
                                                    <a class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Ver" data-original-title="Tooltip on top" href="{{ route('funcionarios.show', $func->id)}}">
                                                        <i class="fa fa-eye"></i></a>
                                                    </div>

                                                        <div class="col-md-4 ">
                                                            <a href="{{ URL::action('FuncionarioController@edit', $func->id)}}" class="btn btn-info" data-toggle="tooltip" data-placement="top"  title="Editar" ><i class="fa fa-edit"></i></a>
                                                        </div>

                                                        <div class="col-md-4">
                                                        {!! Form::model($func, ['method' => 'delete', 'route' => ['funcionarios.destroy', $func->id], 'class' =>'form-inline form-delete']) !!}
                                                            {!! Form::hidden('id', $func->id) !!}
                                                            <button type="submit" name="delete_modal" class="btn btn-danger delete" data-toggle="tooltip" data-placement="top" title="Eliminar" data-original-title="Eliminar"><i class="fa fa-trash"></i></button>

                                                        {!! Form::close() !!}
                                                        </div>

                                                        
                                                    </div>
                                                        
                                                        
                                                </td>
                                        </tr>
                                        
                                @endforeach
                        </table>
                </div>
                
        </div>
</div>
@endsection




