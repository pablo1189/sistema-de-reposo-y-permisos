<style type="text/css">


@page {
       margin-top: 2em;
        margin-left: 4em;
        margin-right: 3em;
        margin-bottom: 6em;
}

@set_paper('letter', 'portrait');

.fuente{

  font-family:helvetica,arial,sans-serif;
  font-size: 10px;
  text-transform: uppercase;
}

.color_celda{
  background-color: #4169E1 !important;
  color: #cecece !important;
  padding-left: 5px !important;
}

.titulos{
  font-size: 15px !important;
  font-weight: bold !important;
}


#mitabla{
/*estilo de el cuerpo de nuestra tabla*/
background-color: #cecece;
width:100%;
margin:24px auto;
border-radius: 5px 2px 5px 2px;

font-family:helvetica,arial,sans-serif;
font-weight:normal;
font-size: 10px;
text-transform: uppercase;
}

#mitabla th{
/*estilo de los elementos(th)*/
background-color: #cecece;
color: white;
font-weight: bold;

/*padding: 8px;*/
}

#mitabla td{

/*estilo a los elementos (td)*/
/*width:100px;
height: 24px;*/
color: #000;
margin: 15px;
padding: 8px;
}

/*Aqui esta la magia ahora vamos usar el  nth-child()**/

#mitabla tr:nth-child(odd){

/*para los colores de las filas impares*/
background-color: #ffffff;
}

#mitabla tr:nth-child(even){

/*para los colores de las filas pares*/
background:#EDEDED;
}

#header {
 margin-bottom: 1em; margin-top: 0em;}

#footer { position: fixed;}

#contenido{
	margin-top: 1em;
}
h2{
	font-family:helvetica,arial,sans-serif;
	font-weight: bold;
	font-size: 16px;
}

p {
	font-family:helvetica,arial,sans-serif;
	font-size: 10px;
}



#footer{
position:fixed;
left: 0px;
bottom: 0px;
right: 0px;
height: 40px;
}
#footer .page:after { 
            content: counter(page, upper-decimal); 
        }

</style>
<div id="footer" >
    <p align="center">
       <i> <b> ¡Guárico... Revolución Irreversible! </b> </i>
     <hr>
      <p align="center">Dirección General de Control y Seguimiento de Gestión de Gobierno <br>
      Calle Roscio c/c Calle Rivas, Edf. Battista, PB <br>
      Telf. 0246-4315881</p>
    </p>
    <p class="page" align="right">Página N°: </p>
</div>

	<div align="center">
   <img src="C:/xampp/htdocs/reper/public/img/siret_md.png" width="350px" height="55px">
   </div>

   <div class="fuente">
<div  align="right">
    Fecha del Reporte:  <b>{{date('d-m-Y')}}</b>
</div>
 <div align="center">
    <h2 class="titulos" align="center">Reporte: {{count($permiso)}}</h2>
    <div  align="right">
    <b>Activo: </b><img src="C:/xampp/htdocs/reper/public/img/bien.png" width="20px" height="15px">&nbsp;&nbsp;
    <b>Finalizado: </b><img src="C:/xampp/htdocs/reper/public/img/mal.png" width="20px" height="15px">
  </div>
  <table id="mitabla" style="page-break-before: auto;" class="table table-striped table-bordered table-hover">
      <thead>
          <tr>
              <th class="color_celda" width="12px">N°</th>
              <th class="color_celda">Funcionario</th>
             
              <th class="color_celda" width="90px">Tipo de Permiso/Reposo</th>
              <th class="color_celda" width="80px">Fecha Inicio</th>
              <th class="color_celda" width="80px">Fecha Limite</th>
              <th class="color_celda">Motivo de la Solicitud</th>
              <th class="color_celda" width="60px">Estatus del Permiso</th>
          </tr>
      </thead>   
    
      <tbody> 
      @php
         $n=1;
      @endphp

    @foreach($permiso as $perm)
      <tr>  
      <td width="12px">{{$n++}}</td> 
      <td>{{$perm->funcionario->nombre_completo}}</td> 
              
      <td width="90px">{{$perm->tipo->nombre }}</td>
      <td width="60px">{{date("d-m-Y",strtotime($perm->fecha_inicio))}}</td>
      <td width="60px" >{{date("d-m-Y",strtotime($perm->fecha_final))}}</td> 
      <td>{{$perm->observacion}} </td>      
      <td width="60px" align="center" >@if($perm->status == 1)
          <img src="C:/xampp/htdocs/reper/public/img/bien.png" width="20px" height="15px">
         @else

            <img src="C:/xampp/htdocs/reper/public/img/mal.png" width="20px" height="15px">
        @endif
      </td>                                 
      </tr>  
     @endforeach
  </tbody>

  </table>
  
  </div>
  </div> 