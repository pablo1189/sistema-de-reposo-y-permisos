@extends('layouts.template')
@section('title')
       SIREP | Permisos
@endsection
@section('content')
<div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">

        

        <h3>Listado de Permisos</h3>
        </div>
</div>
<div class="row">
        <div class="col-md-12">
                <div class="table-responsive">
                        <table id="tabla" class="display" cellspacing="0" width="100%"  class="table table-striped table-condensed table-bordered table-hover" data-form="deleteForm">
                                <thead>
                                        <th width="20px"><b> N°</b></th>
                                        <th>Funcionario</th>
                                        <th>Tipo</th>                                        
                                        <th>Motivo de la Solicitud</th>
                                        <th>Estatus</th>                                      
                                        <th width="130px"><p class="text-center"> Acción</p></th>
                                </thead>
                                
                                    @php
                                    $n=1;
                                    @endphp
                                @foreach($permisos as $perm)
                                        <tr>
                                                 <td> {{$n++}} </td>
                                                <td>{{ $perm->funcionario->nombre_completo }}</td>
                                                <td>{{ $perm->tipo->nombre }}</td>
                                                <td>{{ $perm->observacion}}</td>
                                                <td> 
                                                
                                                    @if($perm->status == 1)

                                                        {{ 'Activo' }}

                                                    @else

                                                        {{'Finalizado'}}

                                                    @endif
                                                
                                                 </td>
                                                                                                
                                                <td>

                                                    <div class="row">
                                                    <div class="col-md-3">
                                                    <a class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Ver" data-original-title="Tooltip on top" href="{{ route('permisos.show', $perm->id) }}">
                                                        <i class="fa fa-eye"></i></a>
                                                    </div>
                                                    
                                                        <div class="col-md-3">
                                                            <a href="{{ URL::action('PermisoController@edit', $perm->id)}}" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Editar" data-original-title="Tooltip on top" title="Editar"><i class="fa fa-edit"></i></a>
                                                        </div>
                                                            
                                                        <div class="col-md-3">
                                                        {!! Form::model($perm, ['method' => 'delete', 'route' => ['permisos.destroy', $perm->id], 'class' =>'form-inline form-delete']) !!}
                                                            {!! Form::hidden('id', $perm->id) !!}
                                                            <button type="submit" name="delete_modal" class="btn btn-danger delete" data-toggle="tooltip" data-placement="top" title="Eliminar" data-original-title="Eliminar"><i class="fa fa-trash"></i></button>
                                                        {!! Form::close() !!}
                                                        </div>

                                                                                                              
                                                </div>
                                                        
                                                        
                                                </td>
                                        </tr>
                                        
                                @endforeach
                        </table>
                        
                </div>
                
        </div>
</div>
@endsection




