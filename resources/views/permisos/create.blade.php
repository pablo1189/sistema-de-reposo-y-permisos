@extends('layouts.template')

@section('title')
        Permisos| Agregar
@endsection

@section('content')

<div class="col-md-8 col-md-offset-2">
    <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Permisos</h3>
      </div>
      <div class="panel-body">

{!! Form::open(['action' => 'PermisoController@store', 'enctype'=>'multipart/form-data']) !!}

	{!! Form::token() !!}

	{{  Form::label('Funcionario') }}

	{!! Form::select('funcionario_id', $funcionario, null, ['class'=>'form-control', 'placeholder' => '-- Seleccione --','required']) !!}


	{{  Form::label('Tipo') }}

	{!! Form::select('tipo_id', $tipo, null, ['class'=>'form-control','placeholder' => '-- Seleccione --',
	 'required']) !!}

    
     {{Form::label('Duración del Permiso')}}

      <?php $duracion = ['dias' => 'Dias Hábiles', 'horas' => 'Por Horas']; ?>
     
    {!! Form::select('duracion', $duracion, null,  ['class'=>'form-control', 'placeholder' => '-- Seleccione --', 'required','id'=>'duracion']) !!}
     
    
    
	{{  Form::label('Fecha de Inicio') }}

	{!! Form::date('fecha_inicio', null, ['class'=>'form-control','placeholder' => 'Fecha Inicial..']) !!}
    
	{{  Form::label('Fecha Limite') }}

	{!! Form::date('fecha_final', null, ['class'=>'form-control','placeholder' => 'Fecha Final..']) !!}
    

    <div id="horas">


    {{  Form::label('Hora Inicial') }}

    {!! Form::text('hora_inicio', null, ['class'=>'form-control','placeholder' => 'Hora Inicial Ej: 10:00', 'id' => 'inicio']) !!}

    {{  Form::label('Hora Final') }}

    {!! Form::text('hora_final', null, ['class'=>'form-control','placeholder' => 'Hora Final Ej: 12:00', 'id'=> 'fin', 'onblur' => 'restarHoras()']) !!}

    {{  Form::label('Cantidad de Horas') }}

    {!! Form::text('cantidad_hora', null, ['class'=>'form-control', 'placeholder' => 'Cantidad de Horas', 'id'=>'resta']) !!}

    
    <script type="text/javascript">
       
    function restarHoras() {

      inicio = document.getElementById("inicio").value;
      fin = document.getElementById("fin").value;
      
      inicioMinutos = parseInt(inicio.substr(3,2));
      inicioHoras = parseInt(inicio.substr(0,2));
      
      finMinutos = parseInt(fin.substr(3,2));
      finHoras = parseInt(fin.substr(0,2));

      transcurridoMinutos = finMinutos - inicioMinutos;
      transcurridoHoras = finHoras - inicioHoras;
      
      if (transcurridoMinutos < 0) {
        transcurridoHoras--;
        transcurridoMinutos = 60 + transcurridoMinutos;
      }
      
      horas = transcurridoHoras.toString();
      minutos = transcurridoMinutos.toString();
      
      if (horas.length < 2) {
        horas = horas + " Horas";
      }
      
      if (horas.length < 2) {
        horas = horas + " Horas";
      }
      
      document.getElementById("resta").value = horas;

    }

    </script>


    </div>


	
    {{  Form::label('Motivo de la Solicitud') }}  

	{!! Form::text('observacion', null, ['class'=>'form-control', 'placeholder' => 'Observación' ]) !!}

	{!! Form::hidden('status', 1)!!}

	{{  Form::label('Imagen') }}

	{!! Form::file('img_scanner',['class' => 'filestyle', 'data-iconName' => 'fa fa-picture-o', 'data-buttonName' => 'btn-primary', 'data-buttonText' => 'Subir Imagen' , 'placeholder'=>'Imagen']) !!}
	<br>

	{!! Form::submit('Guardar', array('class' => 'btn btn-success')) !!}

	{!! Form::reset('Borrar', array('class' => 'btn btn-danger')) !!}


	{{ Form::close()}}



</div>
</div>
</div>

@endsection

<script src="{{asset('js/jquery-1.3.min.js')}}"></script>

<script>
    $(document).ready(function(){
                    $("#duracion").change(function() {
                            if($(this).val() == 'horas') {
                                
                                $("#horas").show();
                                
                            }
                            else{
                                  $("#horas").hide();
                            }
                    });
                            
                            $("#horas").hide();
            });
</script>

