@extends('layouts.template')

@section('title')
        SIREP | DCSGG
@endsection

@section('content')
  
  <div class="page-header">
    <h1 class="text-center"> <i class="fa fa-eye"></i> Permisos 

    @if (isset($anio))
      {{'del año: '.$anio}}
    @endif

    </h1>
  </div>


<div class="row">
<div class="col-md-12">
  <div class="panel-group">
  <div class="panel panel-primary">
    <div class="panel-heading"><h3 class="text-center"> Seleccionar Filtros </h3></div>
  
  
    <div class="panel-body">
        <div class="row">

          {!! Form::open(['method' => 'GET', 'action' => ['PermisoController@consulta'] , 'role' => 'search']) !!}

          <div class="col-md-2">

           <center> {{Form::label('Año ')}}</center>
            
            <select name="anio" id="" class="form-control">
              <option value="">--Seleccionar año--</option>
              
              @for ($i=2017; $i <= date('Y'); $i++)
                <option value="{{$i}}">{{$i}}</option>
              @endfor

            </select>

          </div>

          <div class="col-md-2">
            <center> {{Form::label('Fecha ')}}</center> 
            {!! Form::date('fecha', null, ['class' => 'form-control'])!!}
          </div>
        
        
          <div class="col-md-3">
              <center> {{Form::label('Divisiones')}}</center>
                                        
              {!! Form::select('division_id', $divisiones, null, ['class' => 'form-control', 'placeholder'=>'-- División --','id' => 'division'])!!}
          </div>

          <div class="col-md-3">
                   <center> {{  Form::label('Departamento') }} </center>

                  {!! Form::select('departamento_id', $departamentos, null, ['class' => 'form-control', 'placeholder'=>'-- Departamento --','id' => 'departamento'])!!}


                   <!--  
                    {!! Form::select('departamento_id',['placeholder'=>'-- Departamento -- '], null, ['class' => 'form-control', 'id'=>'departamento'])!!}-->
          </div>

          

        <div class="col-md-2">
         <center> {{Form::label('')}}</center>
          {!! Form::submit('Buscar', array('class' => 'btn btn-info')) !!}
          {!! Form::reset('Reiniciar',array('class' => 'btn btn-danger'))!!}
        </div>

          {!! Form::close() !!}
      </div>
        </div>
     </div>
  </div>
 </div>
 </div>

  <div class="row">
       
   
        <div class="col-md-12">
       
           
             <div class="table-responsive">
                 <table id="tabla" class="display" cellspacing="0" width="100%"  class="table table-striped table-condensed table-bordered table-hover" data-form="deleteForm">
                                <thead>
                                   <th width="20px" style="color: #8B0000"><b> N°</b></th>
                                    @if (empty($funcionario))
                                    <th style="color: #8B0000"><b>Funcionarios</b></th>
                                    @endif
                                    <th style="color:  #8B0000"><b>Cedula</b></th>
                                    <th style="color:  #8B0000"><b> Fecha Inicio </b></th>
                                    <th style="color:  #8B0000"><b>Fecha Limite </b></th>
                                     <th style="color:  #8B0000"><b> Duracion Por Horas</b></th>
                                    <th style="color:  #8B0000"><b>Motivo de la Solicitud</b></th>
                                    <th style="color:  #8B0000"><b> Estatus </b></th>
                                     
                                </thead>

                                 @php
                                    $n=1;
                                 @endphp
                                @foreach ($permisos as $perm)
                                  <tr>
                                      <td> {{$n++}} </td>
                                  @if (empty($funcionario))
                                      <td>{{$perm->funcionario->nombre_completo}}</td>
                                  @endif
                                       <td>{{$perm->funcionario->cedula }}</td>
                                      <td>{{date("d-m-Y",strtotime($perm->fecha_inicio))}}</td>
                                      <td> {{date("d-m-Y",strtotime($perm->fecha_final))}}</td>
                                      <td>{{$perm->cantidad_hora }}</td>
                                      <td> {{$perm->observacion}} </td>
                                      <td>  @if($perm->status == 1)
                                               {{ 'Activo' }}
                                             @else
                                               {{'Finalizado'}}
                                              @endif
                                      </td>                                        
      
</tr>
@endforeach
    </tbody>

  </table>

  </div>

  
@endsection