@extends('layouts.template')
@section('title')
        SIREP | Funcionario
@endsection

@section('content')

<div class="container">

  @if (Auth::User()->rol ==1)
      
  <div class="col-md-6 col-md-offset-3">
    <div style="background: color: black" class="panel panel-info">
      <div class="panel-heading">
        <h3 style="color: black" class="panel-title">
             Consulta
          <i class="fa fa-search"></i>
        </h3>
         @if (empty($funcionario))
        <div align="right">
          <a class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Imprimir" data-original-title="Tooltip on top" href="{{url('pdf3')}}"><i class="fa fa-print" aria-hidden="true"></i></a>
        </div>

        @else


        @endif
      </div>


      <div class="panel-body">
        <div class="col-md-8 col-md-offset-2">       
          
          <b>
            {!! Form::open(['action' => ['PermisoController@resultado'], 'method' => 'GET']) !!}
            {{  Form::label('Funcionario') }}
            {{Form::select('funcionario_id', $funcionarios, null, ['class' => 'form-control','placeholder' => '-- Seleccione --', 'required'])}}
            <br>

            {{Form::submit('Buscar', ['class' => 'btn btn-success'])}}

            <a href="{{route('resultado')}}" class="btn btn-danger">Reiniciar</a>

            {!! Form::close() !!}
          </b>

        </div>
    
      </div>
    </div>

    </div>
    
    @endif

<br><br>
    @if (!empty($funcionario))

    <div class="col-md-10 col-md-offset-1">
      <div class="page-header">
        <h3 class="text-center"> 
        {{ 'Historial de Permisos de: '.strtoupper($funcionario->nombre_completo)}}
          
          <a class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Imprimir" data-original-title="Tooltip on top" href="{{route('permisos.pdf2',$funcionario->id)}}"><i class="fa fa-print" aria-hidden="true"></i></a>

        </h3>
      </div>
    </div>

    @endif
    
<div class="row">
        <div class="col-md-12">
             <div class="table-responsive">
                 <table id="tabla" class="display" cellspacing="0" width="100%"  class="table table-striped table-condensed table-bordered table-hover" data-form="deleteForm">
                                <thead>
                                   <th width="20px" style="color: #8B0000"><b> N°</b></th>
                                    @if (empty($funcionario))
                                    <th style="color: #8B0000"><b>Funcionarios</b></th>
                                    @endif
                                    <th style="color:  #8B0000"><b>Cedula</b></th>
                                    <th style="color:  #8B0000"><b> Fecha Inicio </b></th>
                                    <th style="color:  #8B0000"><b>Fecha Limite </b></th>
                                     <th style="color:  #8B0000"><b> Duracion Por Horas</b></th>
                                    <th style="color:  #8B0000"><b>Motivo de la Solicitud</b></th>
                                    <th style="color:  #8B0000"><b> Estatus </b></th>
                                     <th style="color:  #8B0000"><b> Acción </b></th>
                                </thead>

                                 @php
                                    $n=1;
                                 @endphp
                                @foreach ($permisos as $perm)
                                  <tr  

                                    @if($perm->status == 1)
                                        style="background-color: #fd747a"
                                    @endif>
                                      <td> {{$n++}} </td>
                                  @if (empty($funcionario))
                                      <td><b>{{$perm->funcionario->nombre_completo}}</b></td>
                                  @endif
                                      <td>{{$perm->funcionario->cedula }}</td>
                                      <td>{{date("d-m-Y",strtotime($perm->fecha_inicio))}}</td>
                                      <td> {{date("d-m-Y",strtotime($perm->fecha_final))}}</td>
                                      <td>{{$perm->cantidad_hora }}</td>
                                      <td> {{$perm->observacion}} </td>
                                      <td>  @if($perm->status == 1)
                                              <b> {{ 'Activo' }}
                                             @else
                                               {{'Finalizado'}}</b>
                                              @endif
                                      </td>
                                      <td> <div class="row">
                                                    <div class="col-md-3">
                                                    <a class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Ver" data-original-title="Tooltip on top" href="{{ route('permisos.show', $perm->id) }}">
                                                        <i class="fa fa-eye"></i></a>
                                                    </div></td>
                                  </tr>  
                                @endforeach                                
                                               
                        </table>
                        
                </div>
                
        </div>
</div>













	<table class="table table-bordered table-striped" id="data" class="display">

		<tr>
      
		</tr>
    
      <tbody> 

         

         

        </tbody>

        </table>
         
        </div>

</div>


  
  <!--<div class="row">
    <div class="col-md-10 col-md-offset-1">
      <h2 class="text-center text-danger">
        <i class="fa fa-info-circle fa-2x"></i>  {$perm->funcionario->nombre_completo . ' No Ha Solicitado Permisos'}}
      </h2>
    </div>
  </div>-->
    
@endsection