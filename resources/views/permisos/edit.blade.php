@extends('layouts.template')
@section('title')
        Editar | Permiso
@endsection
@section('content')
<div class="row">
        <div class="col-md-8 col-md-offset-2">
                <h3>Editar Permiso: {{ $permiso->nombre }}</h3>
                @if(count($errors)>0)
                <div class="alert alert-danger">
                        <ul>
                                @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                @endforeach
                        </ul>
                </div>
                @endif
        </div>
</div>

<div class="col-md-8 col-md-offset-2">
    <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Editar</h3>
      </div>
      <div class="panel-body">
{!! Form::model($permiso, ['method' => 'PUT', 'route' =>['permisos.update', $permiso->id], 'enctype'=>'multipart/form-data']) !!}

{{ Form::token()}}

<div class="col-md-8 col-md-offset-2">
    <h3></h3>

<div class="form-group">
{{  Form::label('Funcionario:') }}
    {!! Form::select('funcionario_id', $funcionario, null, ['class'=>'form-control', 'placeholder' => '-- Seleccione --','required']) !!}
</div>

<div class="form-group">
{{  Form::label('Tipo') }}

    {!! Form::select('tipo_id', $tipo, null, ['class'=>'form-control','placeholder' => '-- Seleccione --',
     'required']) !!}
</div>

<div class="form-group">
{{  Form::label('Fecha de Inicio') }}

    {!! Form::date('fecha_inicio', null, ['class'=>'form-control','placeholder' => 'Fecha Inicial..']) !!}
</div>

<div class="form-group">
{{  Form::label('Fecha de Culminacion') }}

{!! Form::date('fecha_final', null, ['class'=>'form-control','placeholder' => 'Fecha Final..']) !!}
</div>






<div class="form-group">
{{  Form::label('Motivo de la Solicitud') }}  

{!! Form::text('observacion', null, ['class'=>'form-control', 'placeholder' => 'Motivo de la Solicitud...' ]) !!}
</div>

    {!! Form::hidden('status', 1)!!}


<div class="form-group">
{{  Form::label('Imagen') }}

{!! Form::file('img_scanner',['class' => 'filestyle', 'data-iconName' => 'fa fa-picture-o', 'data-buttonName' => 'btn-primary', 'data-buttonText' => 'Subir Imagen' , 'placeholder'=>'Imagen']) !!}
</div>
   

{!! Form::submit('Guardar', array('class' => 'btn btn-success')) !!}

{!! Form::close() !!}

</div>
</div>
</div>
</div>
@endsection
