	<style type="text/css">

@page {
        margin-top: 2em;
        margin-left: 2em;
}


#mitabla{
/*estilo de el cuerpo de nuestra tabla*/
background-color: white;
width:100%;
margin:24px auto;
border-radius: 5px 2px 5px 2px;

font-family:helvetica,arial,sans-serif;
font-weight:normal;
font-size: 10px;
text-transform: uppercase;
}

#mitabla th{
/*estilo de los elementos(th)*/
background-color: #B0C4DE;

font-weight: bold;
font-size:10px;
/*padding: 8px;*/
}

#mitabla td{

/*estilo a los elementos (td)*/
/*width:100px;
height: 24px;*/
color: #000;
margin: 15px;
padding: 8px;
}

/*Aqui esta la magia ahora vamos usar el  nth-child()**/

#mitabla tr:nth-child(odd){

/*para los colores de las filas impares*/
background-color: #ffffff;
}

#mitabla tr:nth-child(even){

/*para los colores de las filas pares*/
background:#EDEDED;
}

#header {
 margin-bottom: 1em; margin-top: 0em;}

#footer { position: fixed;}

#contenido{
	margin-top: 1em;
}
h2{
	font-family:helvetica,arial,sans-serif;
	font-weight: bold;
	font-size: 16px;
}

p {
	font-family:helvetica,arial,sans-serif;
	font-size: 10px;
}

.fuente{

  font-family:helvetica,arial,sans-serif;
  font-size: 10px;
  text-transform: uppercase;

}
#mitabla1{
/*estilo de el cuerpo de nuestra tabla*/
background-color: black;
width:40%;
margin:24px auto;
border-left: 1px solid #ccc;
border-right: 1px solid #ccc;
font-family:helvetica,arial,sans-serif;
font-weight:normal;
font-size: 10px;
text-transform: uppercase;
}

#mitabla1 th{
/*estilo de los elementos(th)*/
background-color: black;
color: #fff;
font-weight: bold;
font-size:10px;
/*padding: 8px;*/
}

#mitabla1 td{

/*estilo a los elementos (td)*/
/*width:100px;
height: 24px;*/
color: #000;
margin: 15px;
padding: 8px;
}

/*Aqui esta la magia ahora vamos usar el  nth-child()**/

#mitabla1 tr:nth-child(odd){

/*para los colores de las filas impares*/
background-color: #ffffff;
}

#mitabla1 tr:nth-child(even){

/*para los colores de las filas pares*/
background:#EDEDED;
}

#footer{
position:fixed;
left: 0px;
bottom: 0px;
right: 0px;
height: 40px;
}
#footer .page:after { 
            content: counter(page, upper-decimal); 
        }


</style>

<div align="center">
   <img src="C:/xampp/htdocs/reper/public/img/siret_md.png" width="350px" height="80px">
</div>

<div class="fuente" align="right">
    Fecha del Reporte:<b>{{date('d-m-Y')}}</b>
</div>

<div class="fuente" align="center">
       
    <h3><i>Sistema De Reposos y Permisos.</i></h3>
    
</div>

  <div class="panel-body">
  <div>
    <table id="mitabla" align="center" class="fuente" class="table table-striped table-bordered table-hover">
         <tr>
           <th colspan='6'><h3>DATOS DEL FUNCIONARIO</h3></th>

         </tr>
          <tr>
            
            <th><h4 width="80px">Cédula:</h4></th><td align="center" > {{$permiso->funcionario->cedula}}</td>
            <th><h4 width="80px">Nombre:</h4></th><td align="center"> {{$permiso->funcionario->nombre}}</td>
            <th><h4 width="80px">Apellido:</h4></th><td align="center" width="180px"> {{$permiso->funcionario->apellido}}</td>
            
          </tr>
          <tr>
            
            <th colspan="6"><h3>DEPENDENCIAS: Dirección General de Control y Seguimiento de Gestión de Gobierno</h3></th><br>
          </tr>
            <tr>
            
            <th><h4 width="80px">Division:</h4></th>
            <td> {{$permiso->funcionario->division->nombre}}</td>
            <th><h4 width="80px">Departamento:</h4></th>
            <td> {{$permiso->funcionario->departamento->nombre}}</td>
            <th><h4 width="80px">Cargo:</h4></th>
            <td> {{$permiso->funcionario->cargo}}</td>
          </tr>
          <tr>
            <th colspan="6"><h3>DURACIÓN</h3></th>

          </tr>
                 
          <tr>
          
            <th><h4 width="80px"> Fecha Inicio: </h4></th>
            <td> {{date("d-m-Y",strtotime($permiso->fecha_inicio))}}</td>
            <th><h4 width="80px"> Fecha Limite: </h4></th>
            <td> {{date("d-m-Y",strtotime($permiso->fecha_final))}}</td>
            <th><h4 width="80px">Duración Por Horas</h4></th>
            <td>{{$permiso->cantidad_hora}}</td>
          </tr>
          <tr>
            <th colspan="6"><h3>PERMISO/REPOSO</h3></th>

          </tr>
      
          <tr>
          
            <th><h4 width="80px">Tipo de Permiso:</h4></th>
            <td> {{$permiso->tipo->nombre}}</td>
            <th><h4 width="80px">Motivo de la Solicitud:</h4></th>
            <td width="180px"> {{$permiso->observacion}} </td>
            <th><h4 width="80px"> Estatus:</h4></th>
          <td>
               @if($fecha_actual > $permiso->fecha_final)
                     {{'Finalizado'}}
               @else
                     {{'Activo'}}
              @endif 
                                        
          </td>
          </tr>         

          
        </table>

    <table id="mitabla" align="center" class="fuente" class="table table-striped table-bordered table-hover" border="1">
        <tr>
            
      <td align="center" width="340px"><br><br><br></td><td align="center" width="340px"><br><br><br></td>
      </tr><tr>
      <th width="340px"><h5>FIRMA DEL EMPLEADO</h5></th><th width="340px"><h5>APROBADO</h5></th>
          </tr>

          <tr>

            <td align="center" width="340px"><br><br><br></td><td align="center" width="340px"><br><br><br></td>

          </tr><tr>
            <th width="340px"><h5>FIRMA DEL JEFE INMEDIATO</h5></th><th width="340px"><h5>FIRMA DE LA DIRECTORA GENERAL DE RECURSOS HUMANOS</h5></th>

          </tr>

    </table>

       
  <div id="footer">
  <div align="center">
    <p style="font-size: 10px;">
      <i>¡Guárico... Revolución Irreversible!</i><br>
      Dirección General de Control y Seguimiento de Gestión de Gobierno <br>
      Calle Roscio c/c Calle Rivas, Edf. Battista, PB <br>
      Telf. 0246-4315881
       <p class="page" align="right">Página </p>
    </p>
   
  </div>
</div>
         
        
    
      
    
           
    

