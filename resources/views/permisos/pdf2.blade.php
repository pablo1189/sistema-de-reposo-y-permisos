	<style type="text/css">

@page {
         margin-top: 2em;
        margin-left: 4em;
        margin-right: 3em;
        margin-bottom: 5em;
}


#mitabla{
/*estilo de el cuerpo de nuestra tabla*/
background-color:#cecece;
width:100%;
margin:24px auto;
font-family:helvetica,arial,sans-serif;
font-weight:normal;
font-size: 10px;
text-transform: uppercase;
}

.fuente{

  font-family:helvetica,arial,sans-serif;
  font-size: 10px;
  text-transform: uppercase;

}

#mitabla th{
/*estilo de los elementos(th)*/
background-color: #B0C4DE;
color: black;
font-weight: bold;
font-size:10px;
/*padding: 8px;*/
}

#mitabla td{

/*estilo a los elementos (td)*/
/*width:100px;
height: 24px;*/
color: #000;
margin: 15px;
padding: 8px;
}

/*Aqui esta la magia ahora vamos usar el  nth-child()**/

#mitabla tr:nth-child(odd){

/*para los colores de las filas impares*/
background-color: #ffffff;
}

#mitabla tr:nth-child(even){

/*para los colores de las filas pares*/
background:#EDEDED;
}

#header {
 margin-bottom: 1em; margin-top: 0em;}

#footer { position: fixed;}

#contenido{
	margin-top: 1em;
}
h2{
	font-family:helvetica,arial,sans-serif;
	font-weight: bold;
	font-size: 16px;
}

p {
	font-family:helvetica,arial,sans-serif;
	font-size: 10px;
}


#mitabla1{
/*estilo de el cuerpo de nuestra tabla*/
background-color: black;
width:40%;
margin:24px auto;
border-left: 1px solid #ccc;
border-right: 1px solid #ccc;
font-family:helvetica,arial,sans-serif;
font-weight:normal;
font-size: 10px;
text-transform: uppercase;
}

#mitabla1 th{
/*estilo de los elementos(th)*/
background-color: black;
color: #fff;
font-weight: bold;
font-size:10px;
/*padding: 8px;*/
}

#mitabla1 td{

/*estilo a los elementos (td)*/
/*width:100px;
height: 24px;*/
color: #000;
margin: 15px;
padding: 8px;
}

/*Aqui esta la magia ahora vamos usar el  nth-child()**/

#mitabla1 tr:nth-child(odd){

/*para los colores de las filas impares*/
background-color: #ffffff;
}

#mitabla1 tr:nth-child(even){

/*para los colores de las filas pares*/
background:#EDEDED;
}

#footer{
position:fixed;
left: 0px;
bottom: 0px;
right: 0px;
height: 40px;
}
#footer .page:after { 
            content: counter(page, upper-decimal); 
        }

</style>

<div id="footer" >
    <p align="center">
       <i> <b> ¡Guárico... Revolución Irreversible! </b> </i>
     <hr>
      <p align="center">Dirección General de Control y Seguimiento de Gestión de Gobierno <br>
      Calle Roscio c/c Calle Rivas, Edf. Battista, PB <br>
      Telf. 0246-4315881</p>
    </p>
    <p class="page" align="right">Página N°: </p>
</div>


<div align="center">
   <img src="C:/xampp/htdocs/reper/public/img/siret_md.png" width="450px" height="70px">
</div>

<div class="fuente" align="right">
    Fecha del Reporte:<b>{{date('d-m-Y')}}</b>
</div>

<div class="fuente" align="center">
    <h3><i>Sistema De Reposos y Permisos.</i></h3>
    <h2 align="center">Historial de Permisos: {{ $funcionario->nombre_completo }}.
  
     </h2>

  <table class="fuente" align="center" >
    <tr>
      <td width="180"><b>Cargo:</b>{{$funcionario->cargo}}</td><td width="180"><b>Departamento:</b>{{$funcionario->departamento->nombre}}</td><td width="180"><b>Division:</b>{{$funcionario->division->nombre}}</td>
    </tr>      
  </table>

 <table class="fuente"  id="mitabla" align="center">
      <thead>
          <tr>
              
              <th style="background-color: #4169E1; color: white;" width="60"><h4>Tipo de Permiso/Reposo</h4></th>
              <th style="background-color: #4169E1; color: white;" width="40"><h4>Fecha Inicio</h4></th>
              <th style="background-color: #4169E1; color: white;" width="40"><h4>Fecha Limite</h4></th>
              <th style="background-color: #4169E1; color: white;" width="60"><h4> Duracion Por Horas</h4></th>
              <th style="background-color: #4169E1; color: white;"><h4>Motivo de la Solicitud</h4></th>
              <th style="background-color: #4169E1; color: white;" width="42"><h4>Estatus</h4></th>
          </tr>
      </thead>   
    
      <tbody> 

    @foreach($permiso as $perm)
      <tr>                
      <td width="60" align="center">{{ $perm->tipo->nombre }}</td>
      <td width="40" align="center"> {{date("d-m-Y",strtotime($perm->fecha_inicio))}}</td>
      <td width="40" align="center"> {{date("d-m-Y",strtotime($perm->fecha_final))}}</td> 
      <td width="60" align="center">{{$perm->cantidad_hora }}</td>
      <td>{{$perm->observacion}} </td>      
      <td width="42" align="center">
          @if($perm->status == 1)
           <img src="C:/xampp/htdocs/reper/public/img/bien.png" width="20px" height="15px">
          @else
            <img src="C:/xampp/htdocs/reper/public/img/mal.png" width="20px" height="15px">
          @endif
      </td>                                 
      </tr>  
     @endforeach
  </tbody>

  </table>
    <div  align="right">
    <b>Activo: </b><img src="C:/xampp/htdocs/reper/public/img/bien.png" width="20px" height="15px">&nbsp;&nbsp;
    <b>Finalizado: </b><img src="C:/xampp/htdocs/reper/public/img/mal.png" width="20px" height="15px">
  </div>
  
  </div>
        