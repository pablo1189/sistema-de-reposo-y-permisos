@extends('layouts.template')

@section('title')
        SIREP | DCSGG
@endsection

@section('content')


  <div class="container">
  
  <div class="col-md-8 col-md-offset-2">
    
     <div class="panel panel-info">
     <div class="panel-heading">

      <h2 class="title" style="color: blue">Permisos/Reposos.</h2><p align="right">

          <a class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Generar PDF" data-original-title="Tooltip on top" href="{{route('permisos.pdf', $permiso->id)}}">
          <i class="fa fa-print"></i></a>


          <!--<a class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Historial" data-original-title="Tooltip on top" href="{route('permisos.pdf2',$permiso->funcionario->id)}}"><i class="fa fa-search" aria-hidden="true"></i></a>-->
                                                            


                                                      

                                                        

         </p>
    
    
    
            
        
    </div>
    <div class="panel-body">

    <table class="table table-striped table-bordered table-hover">
         
          <tr>
            <th>Funcionario</th>
            <td> {{$permiso->funcionario->nombre_completo}}</td>
          </tr>
          
            <tr>
            <th>Tipo de Permiso</th>
            <td> {{$permiso->tipo->nombre}}</td>
          </tr>

           <tr>
            <th>Cargo</th>
            <td> {{$permiso->funcionario->cargo}}</td>
          </tr>

           <tr>
            <th>Departamento</th>
            <td> {{$permiso->funcionario->departamento->nombre}}</td>
          </tr>
          
            <tr>
            <th>Division</th>
            <td> {{$permiso->funcionario->division->nombre}}</td>
          </tr>

          <tr>
            <th> Fecha Inicio </th>
            <td> {{date("d-m-Y",strtotime($permiso->fecha_inicio))}}</td>
          </tr>
          
          <tr>
            <th> Fecha Limite </th>
            <td> {{date("d-m-Y",strtotime($permiso->fecha_final))}}</td>
          </tr>
          <tr>
            <th>Duracion Por Horas </th>
            <td>{{$permiso->cantidad_hora}}</td>
          </tr>
          <tr>
            <th>Hora Inicial </th>
            <td>{{$permiso->hora_inicio}}</td>
          </tr>
          <tr>
            <th>Hora Final </th>
            <td>{{$permiso->hora_final}}</td>
          </tr>
          <tr>
            <th>Motivo de la Solicitud</th>
            <td> {{$permiso->observacion}} </td>
          </tr>


          <th> Estatus</th>
          <td>  @if ($fecha_actual > $permiso->fecha_final)
                  {{'Finalizado'}}
                @else
                  {{'Activo'}}
                @endif 
          </td>

          <tr collspan="2" >
          @if(!empty($permiso->img_scanner))
            <th colspan="2" align="center">Archivo Adjunto</th>
          </tr>     

        </table>

        
        <img src="{{asset('uploads/permisos/'.$permiso->img_scanner)}}" alt="" width="700px" height="550px" class="img-rounded" style="border-radius: 6px">
        @else

        @endif

    

     </div> 
     </div>
     </div>
  </div> 

  </div>

   
</div>



@endsection
  