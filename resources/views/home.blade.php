@extends('layouts.template')
@section('title')
        SIREP | DGCSGG
@endsection
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary">
                <div class="panel-heading" align="center">
                <h1><i class="fa fa-hourglass-half" aria-hidden="true"></i>SIREP. | DGCSGG</h1>
                </div>

                <div class="panel-body">

                    Bienvenido..
                

                
<div class="panel panel-info">
    <div class="panel-heading">
      <h4 class="panel-title">
        <!--<a data-toggle="collapse" data-parent="#accordion" href="#mision">--><a><strong><i class="fa fa-eye" aria-expanded="false">Sistema de Reposos y Permisos</i></strong></a>
      </h4>
    </div>
    <div id="mision" class="panel-collapse collapse in">
      <div class="panel-body">
        <h4 align="justify">  Consiste en un sistema de información automatizado, desarrollado por la División de Tecnología de Información y Comunicación de la Dirección General de Control y Seguimiento de Gestión de Gobierno, con el objeto de registrar los reposos y permisos de manera automatizada, otorgados a los funcionarios pertenecientes a la Dirección, manteniendo así un registro actualizado de cada permiso o reposo.</h4>
      </div>
      
  
      <!--<div id=" mision" class="panel-collapse collapse in">-->
      
        <div class="panel-footer panel-danger">División de Tecnología Información y Comunicación.</div>
      <!--</div>-->
    </div>
</div>

      <!--@if(auth::User()->rol == 1)

          
       <script src="{{asset('js/push.min.js')}}"></script>
          <script>
           Push.create("SIREP",{
           	
           body:

           
      "Permisos Activos: "+ {{$activo}} +"  " + "                                            "+

		" Permisos Vencidos: "+ {{$evaluacion}} +" ",
           icon:"/img/alert_caution.png",
           onClick: function()
           {
                window.location="permisos"
                this.close();
            }

          })
          </script>

           @else

           @endif-->
            </div>
        </div>
    </div>
  </div>
</div>
@endsection
