<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      @if(auth::User()->rol == 1)
      
      <a class="navbar-brand" href="{{route('home')}}">  <i class="fa fa-hourglass-half" aria-hidden="true"></i> SIREP</a>
      </div>

      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
      <ul class="nav navbar-nav">

      
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">  <i class="fa fa-calendar-plus-o fa-1x"></i> Permisos<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">

          <li><a href="{{route('permisos.create')}}"><b><i class="fa fa-plus" aria-hidden="true"></i>Crear</b></a></li>

            <li><a  href="{{route('permisos.index')}}"><b><i class="fa fa-clipboard" aria-hidden="true" fa-1x></i>Listado</b></a></li>

            <li><a href="{{route('tipos.index')}}"><b><i class="fa fa-file-text-o" aria-hidden="true" fa-1x></i>Tipos</b></a></li>
            
          </ul>
        </li>

        

        <li>
          <a href="{{route('funcionarios.index')}}"><i class="fa fa-users fa-1x"></i> Funcionario</a>
        </li>

        <li>
          <a href="{{route('divisions.index')}}"><i class="fa fa-sitemap fa-1x" aria-hidden="true"></i> Divisiones</a>
        </li>

        <li>
          <a href="{{route('departamentos.index')}}"><i class="fa fa-cubes fa-1x" aria-hidden="true"></i>  Departamentos</a>
        </li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">  <i class="fa fa-search fa-1x"></i> Consultas<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">

          <li>
          <a href="{{route('resultado')}}"><i class="fa fa-user-o" aria-hidden="true"></i>  Funcionario</a>
        </li>
          <li>
          <a href="{{Url('historial')}}"><i class="fa fa-clock-o fa-1x" aria-hidden="true"></i> Historial</a>
        </li>

        <li>
          <a href="{{Url('consulta')}}"><i class="fa fa-search fa-1x" aria-hidden="true"></i> Consulta General</a>
        </li>

      </ul>
      </li>
       </ul>

        <ul class="nav navbar-nav navbar-right">


                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">Ingresar</a></li>
                            <li><a href="{{ url('/register') }}">Registrate</a></li>
                        @else

                             @if ($activo >=1)
                              <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="glyphicon glyphicon-globe"></span><span class="badge" style="background-color: red; vertical-align: top;"><i class="fa fa-info-circle fa-1x" aria-hidden="true"></i></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                      <a  href="{{route('permisos.index')}}">
                                      <p class="text-center text-info"><b> Permisos Activos: {{$activo}} </b></p></a>
                                    </li>
                                    
                                </ul>
                            </li>

                                @else


                                <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="glyphicon glyphicon-globe"></span>
                                </a>

                                @endif

                            
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">

                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a class="btn btn-danger" href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                           <i class="fa fa-sign-out"></i> Cerrar Sesion
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
        
      @else
      
      <a class="navbar-brand" href="{{route('home')}}">  <i class="fa fa-hourglass-half" aria-hidden="true"></i> SIREP</a>
      </div>

      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
      <ul class="nav navbar-nav">

      
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">  <i class="fa fa-calendar-plus-o fa-1x"></i> Permisos<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">

          <li><a  href="{{route('permisos.create')}}"><b><i class="fa fa-plus" aria-hidden="true"></i>Crear</b></a></li>
                       
          </ul>
        </li>

        <li>
          <a href="{{route('resultado')}}"><i class="fa fa-clock-o fa-1x" aria-hidden="true"></i> Historial</a>
        </li>
        </ul>
         <ul class="nav navbar-nav navbar-right">


                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">Ingresar</a></li>
                            <li><a href="{{ url('/register') }}">Registrate</a></li>
                        @else
                            
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a class="btn btn-danger" href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                           <i class="fa fa-sign-out"></i> Cerrar Sesion
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
        @endif


    </div>
  </div>
</nav>