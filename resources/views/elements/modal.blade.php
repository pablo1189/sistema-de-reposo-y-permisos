<div class="modal" id="confirm" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color:rgb(244, 214, 16)">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h3 class="modal-title text-center">
                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Advertencia
                </h3>
            </div>
            <div class="modal-body">
                <h4 class="text-center">¿Seguro de Eliminar Este Registro?</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-danger" id="delete-btn">Eliminar</button>
                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
