<!DOCTYPE html>
<html lang="es">
<head>
        <title>@yield('title')</title>
       
        <link rel="shortcut icon"  href="{{asset('images/sirep_xs.ico')}}" media="screen">
        <meta charset="utf-8">
       
        <meta name="author" content="Guárico-DCSGG-DTIC-DP">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <link type="text/css" rel="stylesheet" href="{{ asset('font-awesome/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}" media="screen">
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}" media="screen">
        <link rel="stylesheet" href="{{asset('css/dataTables.bootstrap.min.css')}} ">
        <link rel="stylesheet" href="{{asset('css/jquery.dataTables.min.css')}}">

        
        <link rel="stylesheet" href="{{asset('css/bootstrap-datetimepicker.min.css')}}">
        
        
        
</head>
    <body>
        @if (!empty(Auth::User()->id))
            @include('elements.navbar')
        @endif


        
        @if (\Session::has('message'))
            @include('elements.message')
        @endif
        <div class="container">
          @include('elements.modal')
            
          <br>
                @yield('content')

        </div>



    <script src="{{asset('js/jquery.js')}}"></script>
    <script src="{{asset('js/jquery-1.3.min.js')}}"></script>
     <script src="{{asset('js/moment.js')}}"></script>
      <script src="{{asset('js/app.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-filestyle.min.js')}}"></script>
    <script src="{{asset('js/tooltip.js')}}"></script>
    <script src="{{asset('js/dropdown.js')}}"></script>    
    <script src="{{asset('js/datatable.js')}}"></script>
    <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/modal.js')}}"> </script>
    <script src="{{asset('js/bootstrap-datetimepicker.js')}}"></script>

     </body>


</html>
