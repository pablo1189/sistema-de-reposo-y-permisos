@extends('layouts.template')

@section('title')
        Tipos | Agregar
@endsection

@section('content')
<div class="col-md-8 col-md-offset-2">
	<h3>Tipos de Permisos</h3>
{!! Form::open(['action' => 'TipoController@store']) !!}

	{!! Form::token() !!}
<div class="form-group">
	{!! Form::text('nombre', null, ['class' => 'form-control','placeholder' => 'Nombre']) !!}
</div>
	
<div class="form-group">
	{!! Form::text('descripcion', null, ['class' => 'form-control','placeholder' => 'Descripción'])!!}
</div>

	
	{!! Form::submit('Guardar', array('class' => 'btn btn-success')) !!}


{!! Form::close() !!}
</div>


@endsection