@extends('layouts.template')
@section('title')
        Editar | Permiso
@endsection
@section('content')
<div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <h3>Editar Permiso: {{ $tipo->nombre }}</h3>
                @if(count($errors)>0)
                <div class="alert alert-danger">
                        <ul>
                                @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                @endforeach
                        </ul>
                </div>
                @endif
        </div>
</div>

{!! Form::model($tipo, ['method' => 'PUT', 'route' =>['tipos.update', $tipo->id]]) !!}
{{ Form::token()}}
<div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text" name="nombre" required value="{{$tipo->nombre}}" class="form-control">
                </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                        <label for="direccion">Descripcion</label>
                        <input type="text" name="descripcion" value="{{$tipo->descripcion}}" class="form-control" placeholder="Descripcion...">
                </div>
        </div>
        
      
               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                                <button type="submit" class="btn btn-primary">Guardar</button>
                                <button type="reset" class="btn btn-danger">Cancelar</button>
                        </div>
                </div>
                
</div>
{!! Form::close() !!}

@endsection
