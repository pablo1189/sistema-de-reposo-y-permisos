@extends('layouts.template')
@section('title')
       SIREP | Permisos
@endsection
@section('content')
<div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <h3>Tipos de Permisos<a href="{{asset('tipos/create')}}" class="btn btn-success">Nuevo</a></h3>
                
        </div>
</div>
<div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="table-responsive">
                        <table id="tabla" class="display" cellspacing="0" width="100%"  class="table table-striped table-condensed table-bordered table-hover" data-form="deleteForm">
                                <thead>
                                        
                                        <th>Nombre</th>
                                        <th>Descripción</th>
                                        <th align="center">Acción</th>
                                </thead>
                                @foreach($tipos as $tips)
                                        <tr>
                                                
                                                <td>{{ $tips->nombre }}</td>
                                                <td>{{ $tips->descripcion }}</td>
                                                <td>

                                                        <a href="{{ URL::action('TipoController@edit', $tips->id)}}" class="btn btn-info"><i class="fa fa-edit"></i></a>
                                                        <div class="col-md-3">
              											{!! Form::model($tips, ['method' => 'delete', 'route' => ['tipos.destroy', $tips->id], 'class' =>'form-inline form-delete']) !!}
              												{!! Form::hidden('id', $tips->id) !!}
              												<button type="submit" name="delete_modal" class="btn btn-danger delete" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Eliminar"><i class="fa fa-trash"></i></button>
              {!! Form::close() !!}
            </div>
                                                        
                                                </td>
                                        </tr>
                                        
                                @endforeach
                        </table>
                </div>
                
        </div>
</div>
@endsection






